#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script plots MEG data results (Figures 8) from saved files.

@author: MSFabus
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats, io
from statsmodels.sandbox.stats.multicomp import multipletests
import seaborn as sns 
import statsmodels.api as sm
from Tools import it_emd, utils
import emd
from matplotlib import gridspec, artist

## Plotting options
save = True
plt.rcParams['figure.dpi'] = 400
dpi = 600
data_dir = './Data_prebaked/MEG/'
fig_dir = './Figures/'

#Labels
labs = ['itEMD', 'dyadic sift', 'ensemble sift']
cols = ['green', 'darkorange', 'cornflowerblue']

# Load data
data = np.load(data_dir + 'itEMD_MEG_data_10seg.npz')
ph = data['phases']
wfs = data['wfs']
pmsi_raw = data['pmsi']
fpeak = data['f_peak']
niters = data['niters']

#%% Set up figure
plt.rc('font', size=10) 
fig = plt.figure(figsize=(7.0625, 5))
axgrid = (6, 13)
spec = gridspec.GridSpec(ncols=3, nrows=2, width_ratios=[1, 1, 1],
                         wspace=0.5, hspace=0.33)
ax1 = fig.add_subplot(spec[0, 0])
ax2 = fig.add_subplot(spec[0, 1])
ax3 = fig.add_subplot(spec[0, 2])
ax4 = fig.add_subplot(spec[1, 0])
ax5 = fig.add_subplot(spec[1, 1:3])
ax1.annotate('A', xy=(-0.6, 0.33), fontsize=24, annotation_clip=False)
ax1.annotate('B', xy=(3.75, 0.33), fontsize=24, annotation_clip=False)
ax1.annotate('C', xy=(8, 0.33), fontsize=24, annotation_clip=False)
ax1.annotate('D', xy=(-0.6, -0.09), fontsize=24, annotation_clip=False)
ax1.annotate('E', xy=(3.75, -0.09), fontsize=24, annotation_clip=False)

ax1.annotate('**', xy=(1.8, 0.31), fontsize=12, annotation_clip=False)
ax1.annotate('**', xy=(2.35, 0.24), fontsize=12, annotation_clip=False)
ax1.annotate('***', xy=(1.25, 0.27), fontsize=12, annotation_clip=False)

# 8A: PMSI violin plot
pmsi = pmsi_raw.copy()
pmsi = np.delete(pmsi_raw, [2], axis=0)
pvalue = np.zeros(3)*np.nan
_ , pvalue[0] = stats.ttest_rel(pmsi[:, 0], pmsi[:, 1], nan_policy='omit')
_ , pvalue[1] = stats.ttest_rel(pmsi[:, 0], pmsi[:, 2], nan_policy='omit')
_ , pvalue[2] = stats.ttest_rel(pmsi[:, 1], pmsi[:, 2], nan_policy='omit')

pvalue *= 2 #one-tailed result
reject, corrpvalue, alphacSidak, alphacBonf = multipletests(pvalue, alpha=0.05, method='bonferroni')

ax = ax1
violin_parts = ax.violinplot(pmsi, showextrema=False, showmedians=True)
ct = 0
for pc in violin_parts['bodies']:
    pc.set_facecolor(cols[ct])
    pc.set_edgecolor('black')
    pc.set_alpha(1)
    pc.set_linewidth(1)
    ct += 1

violin_parts['cmedians'].set_edgecolor('black')
violin_parts['cmedians'].set_linewidth(1.5)

ax.plot([1, 2], [0.27, 0.27], color='k', lw=2)
ax.plot([1, 3], [0.3, 0.3], color='k', lw=2)
ax.plot([2, 3], [0.24, 0.24], color='k', lw=2)

ax.set_ylabel('PMSI')
ax.set_xticks([1, 2, 3])
ax.set_yticks([0, 0.1, 0.2, 0.3])
ax.set_xticklabels(['itEMD', 'Dyadic', 'EEMD'])
ax.tick_params(axis='x', pad=5)


# 8B: Average phase-aligned IF
wfm = np.nanmean(wfs, axis=0)
wfe = np.nanstd(wfs, axis=0) / np.sqrt(9)

ax = ax2
for k in range(3):
    y1 = wfm[k, :]
    er1 = wfe[k, :]
    ax.plot(ph, y1, label=labs[k], linewidth=2, color=cols[k])
    ax.fill_between(ph, y1-er1, y1+er1, alpha=0.5, color=cols[k])

ax.set_xlabel('Phase [rad]')
ax.set_ylabel('IF [Hz]')
leg = ax.legend(ncol=1, fontsize=7)
for line in leg.get_lines():
    line.set_linewidth(3.0)
axx = ax.add_artist(leg)
ax.set_xticks([0, np.pi, 2*np.pi])
plt.rc('text', usetex=True)
ax.set_xticklabels([0, '$\pi$', '$2 \pi$'])
plt.rc('text', usetex=False)
ax.set_xlim(0, 2*np.pi)
ax.set_ylim(8.5, 11.5)
ax.set_yticks([9, 10, 11])
ax.tick_params(axis='x', pad=5)
ax.grid(True)
ax.set_axisbelow(True) 


# 8D: Peak frequencies in Fourier vs itEMD
ax = ax4
occ_f = fpeak

fpeak_emd = np.mean(wfs, axis=2)
fstd_emd = np.std(wfs, axis=2)

occ_f_emd = fpeak_emd[:, 0]
occ_std_emd = fstd_emd[:, 0]

X2 = sm.add_constant(occ_f)
est = sm.OLS(occ_f_emd, X2, missing='drop')
res = est.fit(); print(res.summary())
ypred = res.predict(X2)

sns.regplot(x=occ_f, y=occ_f_emd, label=labs[0], color=cols[0], ax=ax,
            scatter_kws={'s':5}, line_kws={'lw':2})
ax.set_xlabel('Fourier peak f [Hz]')
ax.set_ylabel('itEMD shape f [Hz]')
ax.set_xticks([8, 9, 10, 11])
ax.set_yticks([8, 9, 10, 11])
ax.set_ylim(8.5, 11.5)
ax.set_xlim(8.5, 11.5)
ax.tick_params(axis='x', pad=5)
ax.grid(True) 
ax.set_axisbelow(True) 


# 8C, 8E: Example spectrum and decomposition
# Load data
fpath = data_dir + 'sub-10_data.mat'
F = io.loadmat(fpath)
data = np.array( F['occ'].flatten())
sample_rate = 400

# Compute itEMD
t = np.linspace(0, len(data)/sample_rate, len(data))
segs = np.linspace(0, t[-1], 11) 
[imf, mask_eq, mask_std, n_it] = it_emd.it_emd_seg(data, t, segs, 
                          N_imf=8, joint=True,
                          N_iter_max=15, N_avg=1, 
                          iter_th=0.1, mask_0='zc', 
                          sample_rate=sample_rate,
                          w_method='power', ignore_last=True)

# 8C: Amplitude weighted HHT plot
IP, IF, IA = emd.spectra.frequency_transform(imf, sample_rate, 'nht')
freq_edges, freq_centres = emd.spectra.define_hist_bins(1, 200, 199, 'log')   
spec_weighted = emd.spectra.hilberthuang_1d(IF, IA, freq_edges)

cs = ['tab:orange', 'tab:pink', 'tab:green', 'tab:brown', 'tab:gray']
cmap = plt.cm.Dark2
cs = cmap(np.linspace(0, 1, 6))[1:]
ax = ax3
ax.set_prop_cycle(color=cs)
ax.loglog(freq_centres, spec_weighted, lw=1.5)
ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel('Energy [A.U.]')
ax.set_ylim(100, 2e5)
leg = ax.legend(['IMF-1', 'IMF-2', 'IMF-3', 'IMF-4', 'IMF-5'], ncol=2,
                fontsize=7, bbox_to_anchor=(1.03, 1.35))
for line in leg.get_lines():
    line.set_linewidth(3.0)
axx = ax.add_artist(leg)
ax.grid(True)
ax.set_axisbelow(True) 

# 8E: Example decomposition
utils.plot_imfs(imf[20*400:25*400, :5], 400, fig, ax5, 
                pos=[0.43, 0.4, 0.45, 0.35], fontsize=6, nticks=5,
                labelpad=3)
if save:
    fname = 'Fig8.tiff'
    fig.savefig(fig_dir + fname, format='tiff', bbox_inches='tight',
        pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()