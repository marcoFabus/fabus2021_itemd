#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script plots supplementary figures from saved files.

@author: MSFabus
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from Tools import it_emd, sim, utils
import emd

## Plotting options
save = False
plt.rcParams['figure.dpi'] = 200
dpi = 200
data_dir = './Data_prebaked/SI/'
fig_dir = './Figures/SI'

#Labels
cols = ['darkorange', 'cornflowerblue', 'green']
labs = ['Dyadic Mask Sift', 'Ensemble Sift', 'itEMD']


#%% Figure S1: Low-noise dip explanation
cases = ['B', 'D', 'A', 'C']
noise = [0.01, 0.1]

sim3 = sim.sim(t=10, sample_rate=512)
f0 = 4
f_avg = 4.214

for i in range(2):
    x = sim3.sim_sig(N_comp=1, amps=[1], f=[f0], shape=['it_sin'], 
                      noise=True, amp_mod=[1], it_sin_order=8,
                      noise_type='white', a_noise=noise[i], seed=0) 
    x0 = sim3.x.T.flatten()
    
    [f, p0] = utils.plot_psd(x0, sim3.sample_rate, return_psd=True)
    [f, ps] = utils.plot_psd(x, sim3.sample_rate, return_psd=True)
    p_all = np.zeros((3, len(p0)))
    
    imf, mask_eq, mask_std, _, _ =    it_emd.it_emd(x, N_imf=6, 
                                              N_iter_max=15, N_avg=1, 
                                              iter_th=0.1, mask_0='zc') 
    ## S1 B / D
    fig = it_emd.plot_imf(imf[:, :5], secs=[8, 10], sample_rate=sim3.sample_rate) 
    if save:
        n1 = 'FigS1' + cases[i] + '.tiff'
        fig.savefig(fig_dir + n1, format='tiff', bbox_inches='tight',
                    pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
    plt.show()
    
    ## S1 A / C
    plt.rc('font', size=32)
    fig2, ax = plt.subplots(figsize=(12,8))
    ax.semilogy(f, ps, color='k', lw=2, label='Iterated Sine + Noise')
    ax.semilogy(f, p0, color='blue', lw=1, label='Iterated Sine')     
    ax.set_xlim([0, 30])
    ax.set_ylim(1e-7, 2)
    ax.set_ylabel('Power Spectrum [a.u.]')
    ax.set_xlabel('Freq [Hz]')
    ax.set_yticks([1e-6, 1e-4, 1e-2, 1])
    ax.grid()
    plt.rc('font', size=20)
    leg = ax.legend(ncol=3, bbox_to_anchor=(0.9, 1.15), loc='upper right')
    for line in leg.get_lines():
        line.set_linewidth(8.0)
        
    if save:
        n2 = 'FigS1' + cases[i+2] + '.tiff'
        fig2.savefig(fig_dir + n2, format='tiff', bbox_inches='tight',
                    pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
    plt.show()
    
    
#%% Figure S2: Spectral Bandwidth Analysis

## S2-A
data = np.load(data_dir + 'itEMD_FigS2A_data.npz')
noise = data['noise']
sef = data['sbw']
[sef_mean, sef_er, H0_rej] = utils.prepare_fig(sef, H1=2, H0=[0,1], comparison='best',
                                           contrast='higher', smoothing_win=3)

plt.rc('font', size=32)
fig, ax = plt.subplots(figsize=(12, 8))

for l in range(3):
    ax.plot(noise, sef_mean[l, :], label=labs[l], color=cols[l])
    ax.fill_between(noise,  sef_mean[l, :] - sef_er[l, :], 
                    sef_mean[l, :] + sef_er[l, :], alpha=0.5, color=cols[l])
ax.plot(noise, H0_rej*10, color='k', linewidth=4)
ax.set_ylabel('Spectral Bandwidth [Hz]')
ax.set_xlabel('RMS Noise')
ax.grid(True)
ax.set_xlim(0, 3)
ax.set_xticks([0, 1, 2, 3])
ax.set_yticks([4, 6, 8, 10])
ax.tick_params(axis='x', pad=15)
plt.rc('font', size=20)
leg = ax.legend(ncol=3, bbox_to_anchor=(1, 1.15), loc='upper right')
for line in leg.get_lines():
    line.set_linewidth(8.0)

if save:
    fig.savefig(fig_dir + 'FigS2A.tiff', format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()


## S2-B: Example Power Spectra
sim3 = sim.sim(t=10, sample_rate=512)
f0 = 4
f_avg = 4.214

x = sim3.sim_sig(N_comp=1, amps=[1], f=[4], shape=['it_sin'], 
                  noise=True, amp_mod=[1], it_sin_order=8,
                  noise_type='white', a_noise=1.5, seed=0) 

p_all = np.zeros((3, 1025))

for k in range(3):
    if k == 0:
        imf = emd.sift.mask_sift(x, max_imfs=6, mask_freqs='zc')
    if k == 1:
        imf = emd.sift.ensemble_sift(x, max_imfs=6)
    if k == 2:
        imf, mask_eq, mask_std, _, _ = it_emd.it_emd(x, N_imf=6, 
                                  N_iter_max=15, N_avg=1, 
                                  iter_th=0.1,
                                  mask_0='zc', sample_rate=512) 
    
    IP,IF,IA = emd.spectra.frequency_transform(imf, sim3.sample_rate, 'nht')
    m = np.nanargmin(np.abs(np.mean(IF,0)-f_avg))
    [f, p_imf] = utils.plot_psd(imf[:,m], sim3.sample_rate, return_psd=True)
    p_all[k, :] = p_imf
    
    # Remove main peaks for spectral edge estimate
    p_all[k, 15:18] = np.mean(p_imf[[13, 14, 18, 19]])
    p_all[k, 47:50] = np.mean(p_imf[[46, 50]])

plt.rc('font', size=32)
fig, ax = plt.subplots(figsize=(12,8))
for k in range(3):
    p = p_all[k, :]
    ax.semilogy(f, p, color=cols[k], label=labs[k], lw=3)
ax.set_xlim([0, 30])
ax.set_ylim(1e-8, 0.05)
ax.set_ylabel('Power Spectrum [a.u.]')
ax.set_xlabel('Freq [Hz]')
ax.set_yticks([1e-8, 1e-6, 1e-4, 1e-2])
ax.grid()
plt.rc('font', size=20)
leg = ax.legend(ncol=3, bbox_to_anchor=(1, 1.15), loc='upper right')
for line in leg.get_lines():
    line.set_linewidth(8.0)
if save:
    n2 = 'FigS2B.tiff'
    fig.savefig(fig_dir + n2, format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()


## S2-C: SNR Boost Figure
data = np.load(data_dir + 'itEMD_FigS2C_data.npz')
noise = data['noise']
snr = data['SNR']
snr0 = data['SNR_0']
[snr_mean, snr_er, H0_rej] = utils.prepare_fig(snr, H1=2, H0=[0,1], comparison='best',
                                           contrast='higher', smoothing_win=3)

plt.rc('font', size=32)
fig, ax = plt.subplots(figsize=(12, 8))
ax.semilogy(noise, snr0, label='Original', color='k') 

for l in range(3):
    ax.plot(noise, snr_mean[l, :], label=labs[l], color=cols[l])
    ax.fill_between(noise,  snr_mean[l, :] - snr_er[l, :], 
                    snr_mean[l, :] + snr_er[l, :], alpha=0.5, color=cols[l])
ax.plot(noise, H0_rej*400, color='k', linewidth=4)
ax.set_ylabel('Burst SNR')
ax.set_xlabel('RMS Noise')
ax.grid(True)
ax.set_xlim(0, 3)
ax.set_xticks([0, 1, 2, 3])
ax.tick_params(axis='x', pad=15)
plt.rc('font', size=20)
leg = ax.legend(ncol=2, bbox_to_anchor=(0.85, 1.2), loc='upper right')
for line in leg.get_lines():
    line.set_linewidth(8.0)
if save:
    fig.savefig('FigS2C.tiff', format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()


#%% Figure S3: Left pannel, noise
## S3-A: Pearson r figure
data = np.load(data_dir + 'itEMD_FigS3_noise_data.npz')
noise = data['noise']
corr = data['corr']
[corr_mean, corr_er, H0_rej] = utils.prepare_fig(corr, H1=2, H0=[0,1], comparison='best',
                                           contrast='higher', smoothing_win=3,
                                           p_lim=0.01)

plt.rc('font', size=32)
fig, ax = plt.subplots(figsize=(12, 8))

for l in range(3):
    ax.plot(noise, corr_mean[l, :], label=labs[l], color=cols[l])
    ax.fill_between(noise,  corr_mean[l, :] - corr_er[l, :], 
                    corr_mean[l, :] + corr_er[l, :], alpha=0.5, color=cols[l])
ax.plot(noise, H0_rej*1, color='k', linewidth=4)
ax.set_ylabel('Pearson r')
ax.set_xlabel('RMS Noise')
ax.grid(True)
ax.set_xlim(0, 3)
ax.set_ylim(-1.1, 1.1)
ax.set_xticks([0, 1, 2, 3])
ax.set_yticks([-1, -0.5, 0, 0.5, 1])
ax.tick_params(axis='x', pad=15)
plt.rc('font', size=20)
leg = ax.legend(ncol=3, bbox_to_anchor=(1, 1.15), loc='upper right')
for line in leg.get_lines():
    line.set_linewidth(8.0)
axx = plt.gca().add_artist(leg)

if save:
    fig.savefig(fig_dir + 'FigS3A.tiff', format='tiff', 
                bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()

## S3-D: PMSI figure
pmsi = data['pmsi']
[pmsi_mean, pmsi_er, H0_rej] = utils.prepare_fig(pmsi, comparison='best', 
                                           contrast='lower', H1=2, H0=[0,1])
plt.rc('font', size=32)
fig, ax = plt.subplots(figsize=(12, 8))

for l in range(3):
    ax.plot(noise, pmsi_mean[l, :], label=labs[l], color=cols[l])
    ax.fill_between(noise,  pmsi_mean[l, :] - pmsi_er[l, :], 
                    pmsi_mean[l, :] + pmsi_er[l, :], alpha=0.5, color=cols[l])
ax.plot(noise, H0_rej*0.3, color='k', linewidth=4)
ax.set_ylabel('PMSI')
ax.set_xlabel('RMS Noise')
ax.grid(True)
ax.set_xlim(0, 3)
ax.set_ylim(-0.01, 0.31)
ax.set_xticks([0, 1, 2, 3])
ax.set_yticks([0, 0.1, 0.2, 0.3])
ax.tick_params(axis='x', pad=15)
plt.rc('font', size=20)
leg = ax.legend(ncol=3, bbox_to_anchor=(1, 1.15), loc='upper right')
for line in leg.get_lines():
    line.set_linewidth(8.0)
axx = plt.gca().add_artist(leg)

if save:
    fig.savefig(fig_dir + 'FigS3D.tiff', format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()


#%% Figure S3: Middle pannel, shape distortion
## S3-B: Pearson r figure
data = np.load(data_dir + 'itEMD_FigS3_FD_data.npz')
fd = data['freq_dist']
corr = data['corr']
[corr_mean, corr_er, H0_rej] = utils.prepare_fig(corr, H1=2, H0=[0,1], comparison='best',
                                           contrast='higher', smoothing_win=3,
                                           p_lim=0.01)
plt.rc('font', size=32)
fig, ax = plt.subplots(figsize=(12, 8))


for l in range(3):
    ax.plot(fd, corr_mean[l, :], label=labs[l], color=cols[l])
    ax.scatter(fd, corr_mean[l, :], color=cols[l])
    ax.fill_between(fd,  corr_mean[l, :] - corr_er[l, :], 
                    corr_mean[l, :] + corr_er[l, :], alpha=0.5, color=cols[l])
ax.plot(fd, H0_rej*1, color='k', linewidth=4)
ax.set_ylabel('Pearson r')
ax.set_xlabel('Frequency Distortion [%]')
ax.grid(True)
ax.set_ylim(-0.25, 1.1)
ax.set_yticks([0, 0.5, 1])
ax.tick_params(axis='x', pad=15)
plt.rc('font', size=20)

leg = ax.legend(ncol=3, bbox_to_anchor=(1, 1.15), loc='upper right')
for line in leg.get_lines():
    line.set_linewidth(8.0)

if save:
    fig.savefig(fig_dir + 'FigS3B.tiff', format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()

## S3-E: PMSI figure
pmsi = data['pmsi']
[pmsi_mean, pmsi_er, H0_rej] = utils.prepare_fig(pmsi, comparison='best', 
                                           contrast='lower', H1=2, H0=[0,1])

plt.rc('font', size=32)
fig, ax = plt.subplots(figsize=(12, 8))

for l in range(3):
    ax.plot(fd, pmsi_mean[l, :], label=labs[l], color=cols[l])
    ax.scatter(fd, pmsi_mean[l, :], color=cols[l])
    ax.fill_between(fd,  pmsi_mean[l, :] - pmsi_er[l, :], 
                    pmsi_mean[l, :] + pmsi_er[l, :], alpha=0.5, color=cols[l])
ax.plot(fd, H0_rej*0.3, color='k', linewidth=4)
ax.set_ylabel('PMSI')
ax.set_xlabel('Frequency Distortion [%]')
ax.grid(True)
ax.set_yticks([0, 0.1, 0.2, 0.3])
ax.tick_params(axis='x', pad=15)
plt.rc('font', size=20)
leg = ax.legend(ncol=3, bbox_to_anchor=(1, 1.15), loc='upper right')
for line in leg.get_lines():
    line.set_linewidth(8.0)
    
if save:
    fig.savefig(fig_dir + 'FigS3E.tiff', format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()
        
        
#%% Figure S3: Right pannel, sparsity
## S3-C: Pearson r figure
data = np.load(data_dir + 'itEMD_FigS3_sparsity_data.npz')
num_osc = data['num_osc']
corr = data['corr']
[corr_mean, corr_er, H0_rej] = utils.prepare_fig(corr, H1=2, H0=[0,1], comparison='best',
                                           contrast='higher', smoothing_win=3)
plt.rc('font', size=32)
fig, ax = plt.subplots(figsize=(12, 8))

for l in range(3):
    ax.plot(num_osc, corr_mean[l, :], label=labs[l], color=cols[l])
    ax.fill_between(num_osc,  corr_mean[l, :] - corr_er[l, :], 
                    corr_mean[l, :] + corr_er[l, :], alpha=0.5, color=cols[l])
ax.plot(num_osc, H0_rej*1, color='k', linewidth=4)
ax.set_ylabel('Pearson r')
ax.set_xlabel('Number of Cycles in Burst')
ax.grid(True)
ax.set_ylim(-0.25, 1.1)
ax.set_yticks([0, 0.5, 1])
ax.set_xlim(0, 100)
ax.tick_params(axis='x', pad=15)
plt.rc('font', size=20)
leg = ax.legend(ncol=3, bbox_to_anchor=(1, 1.15), loc='upper right')
for line in leg.get_lines():
    line.set_linewidth(8.0)
if save:
    fig.savefig(fig_dir + 'FigS3C.tiff', format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()

## S3-F: PMSI figure
pmsi = data['pmsi']
[pmsi_mean, pmsi_er, H0_rej] = utils.prepare_fig(pmsi, comparison='best', 
                                           contrast='lower', H1=2, H0=[0,1])

plt.rc('font', size=32)
fig, ax = plt.subplots(figsize=(12, 8))

for l in range(3):
    ax.plot(num_osc, pmsi_mean[l, :], label=labs[l], color=cols[l])
    ax.fill_between(num_osc,  pmsi_mean[l, :] - pmsi_er[l, :], 
                    pmsi_mean[l, :] + pmsi_er[l, :], alpha=0.5, color=cols[l])
ax.plot(num_osc, H0_rej*0.3, color='k', linewidth=4)
ax.set_ylabel('PMSI')
ax.set_xlabel('Number of Cycles in Burst')
ax.grid(True)
ax.set_yticks([0, 0.1, 0.2, 0.3])
ax.set_xlim(0, 100)
ax.tick_params(axis='x', pad=15)
plt.rc('font', size=20)
leg = ax.legend(ncol=3, bbox_to_anchor=(1, 1.15), loc='upper right')
for line in leg.get_lines():
    line.set_linewidth(8.0)
if save:
    fig.savefig(fig_dir + 'FigS3F.tiff', format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()


#%% Figure S4: Individual brown noise simulation examples
a = np.zeros(30*512)
idx = list(range(int(3*512), int(5.5*512))) #0.25s to frac+0.25s
a[idx] = 1 

secs = [0, 30]
panel = ['A', 'B', 'C']
Nimf = [6, 9, 9]

sim3 = sim.sim(t=30, sample_rate=512)
x = sim3.sim_sig(N_comp=1, amps=[1], f=[4], shape=['it_sin'], 
              noise=True, amp_mod=[a], it_sin_order=8,
              noise_type='brown', a_noise=0.75, seed=0)

for k in range(3): 
    if k == 2:
        imf = emd.sift.mask_sift(x, max_imfs=9, mask_freqs='zc')
    if k == 1:
        imf = emd.sift.ensemble_sift(x, max_imfs=9)
    if k == 0:
        imf, mask_eq, mask_std, _, _ = it_emd.it_emd(x, N_imf=9, 
                                  N_iter_max=15, N_avg=1, 
                                  iter_th=0.1,
                                  mask_0='zc', sample_rate=512) 
    
    fig = it_emd.plot_imf(imf[:, :Nimf[k]], secs=secs, sample_rate=sim3.sample_rate,
                             figsize=(12,12)) 
    if save:
        fig_name = 'FigS4' + panel[k] + '.tiff'
        fig.savefig(fig_dir + fig_name, format='tiff', bbox_inches='tight',
                     pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
    plt.tight_layout(h_pad=0.25)
    plt.show()
        
        
#%% Figure S5: Signal v noise IMF figure
## S5-A: Example IMF decomposition.
sim3 = sim.sim(t=10, sample_rate=512)
x = sim3.sim_sig(N_comp=1, amps=[1], f=[4], shape=['it_sin'], 
             noise=True, amp_mod=[1], it_sin_order=8,
             noise_type='white', a_noise=1.5, seed=97) 

imf, mask_eq, mask_std, niters, flag = it_emd.it_emd(x, N_imf=6, 
                          N_iter_max=15, N_avg=1, 
                          iter_th=0.1,
                          mask_0='zc') 
    
fig = it_emd.plot_imf(imf[:, :5], secs=[1, 3], sample_rate=sim3.sample_rate,
                         scale_y=False) 
if save:
    fig.savefig(fig_dir + 'FigS5A.tiff', format='tiff', 
                bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()


# S5-B: Relative mask difference violin plots
data = np.load(data_dir + 'itEMD_FigS5B_data.npy')
_ , pvalue = stats.ttest_rel(data[:, 3], data[:, 4], nan_policy='omit')

cols = ['green', 'darkorange']
plt.rc('font', size=36)
fig, ax = plt.subplots(figsize=(6, 8))
violin_parts = ax.violinplot(data[:, 3:]*100, showextrema=False, showmedians=True, widths=0.5)
ct = 0
for pc in violin_parts['bodies']:
    pc.set_facecolor(cols[ct])
    pc.set_edgecolor('black')
    pc.set_alpha(1)
    ct += 1

violin_parts['cmedians'].set_edgecolor('black')
violin_parts['cmedians'].set_linewidth(2)
ax.plot([1, 2], [10.5, 10.5], color='k', lw=4)
ax.set_ylabel('Mask Variability [%]')
ax.set_ylim(-0.25, 11)
plt.rc('font', size=24)
ax.set_xticks([1, 2])
ax.set_yticks([0, 5, 10])
ax.set_xticklabels(['Signal', 'Noise'])

ax.tick_params(axis='x', pad=15)
plt.tight_layout()

if save:
    fname = 'FigS5B.tiff'
    fig.savefig(fig_dir + fname, format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)