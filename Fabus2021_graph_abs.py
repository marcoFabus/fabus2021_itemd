import emd
import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage, stats, signal
from statsmodels.sandbox.stats.multicomp import multipletests
from Tools import it_emd, utils
from matplotlib import gridspec, artist

## Plotting options
save = True
plt.rcParams['figure.dpi'] = 400
dpi = 600
data_dir = './Data_prebaked/LFP/'
fig_dir = './Figures/'

#Labels
labs = ['itEMD','Dyadic', 'Ensemble']
cols = ['green','darkorange', 'cornflowerblue']
panels = ['D', 'E', 'F']

# Load data
data = np.load(data_dir + 'itEMD_Fig7_data.npz')
pmsi = data['pmsi'].T
imf_all = data['imf']
wfs = data['wfs']
N_w = data['N_w']
niters = data['niters']
ph = data['ph']
sample_rate = 1250

# Print niters info
print('hc3 # of iterations =  %.0f +/- %.0f' % (np.mean(niters), np.std(niters)))

#%% 1: Raw rat LFP
plt.rc('font', size=10) 
fig, ax = plt.subplots(figsize=(7.0625, 1.5))
imf = imf_all[0, :, :]
sig = np.sum(imf[14*sample_rate:16*sample_rate, :5], axis=1)
ax.plot(sig, lw=1, label='Rat LFP', color='k')
ax.set_xlim(0, 2*sample_rate)
ax.axis('off')
if save:
    fname = 'Fig7_graph_abs_LFP.tiff'
    fig.savefig(fig_dir + fname, format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)


#%% 2: IMF results: EEMD and itEMD

for c, i in enumerate([0, 2]):
    plt.rc('font', size=10) 
    fig, ax = plt.subplots(figsize=(7.0625, 6))
    imf = imf_all[i, :, :]
    utils.plot_imfs(imf[14*sample_rate:16*sample_rate, 2:5], sample_rate, fig,
                    ax, pos=[0, 0.7, 1, 1], fontsize=10, nticks=4,
                    lw=1.75, labelpad=3, xlab=False)
    ax.set_xlim(0, 2*sample_rate)
    
    if save:
        fname = 'Fig7_graph_abs_IMFs' + str(c) + '.tiff'
        fig.savefig(fig_dir + fname, format='tiff', bbox_inches='tight',
                    pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
ax.axis('off')

#%% 3: IF histograms
for c, i in enumerate([0, 2]):
    plt.rc('font', size=42) 
    fig, ax = plt.subplots(figsize=(6., 10))
    imf = imf_all[i, :, :]
    IP, IF, IA = emd.spectra.frequency_transform(imf[14*sample_rate:16*sample_rate, :5], sample_rate, 'nht')
    # IF[IF<0] = np.nan
    # mask = ~np.isnan(IF)
    # filtered_data = [d[m] for d, m in zip(IF.T, mask.T)]
    # ax.violinplot(filtered_data, points=300)
    # ax.set_yscale('log')
    # continue
    # Average freq boxplot
    edge_color = 'black'
    #data = IF_seg[2, :, :]
    bx = ax.boxplot(IF[:, 2:], patch_artist=True, widths=0.85, showfliers=False, whis=[10, 90])
    
    # ax.plot([1, 2], [20, 20], c='k', lw=1)
    # ax.plot([2, 3], [8, 8], c='k', lw=1)
    # ax.plot([3, 4], [3, 3], c='k', lw=1)
    
    ax.set_yscale('log')
    ax.set_yticks([2, 4, 8, 16, 32, 64])
    ax.set_yticklabels( [2, 4, 8, 16, 32, 64])
    ax.minorticks_off()
    
    colors = ['#FF5BDA', 'cornflowerblue', '#F39C12', 'firebrick']
    cmap = plt.cm.Dark2
    colors = cmap(np.linspace(0, 1, 6))[1:, :]#imfs.shape[1] + 1))

    for element in ['whiskers', 'fliers', 'means', 'medians', 'caps']:
        plt.setp(bx[element], color=edge_color)
        
    for patch, color in zip(bx['boxes'], colors):
        patch.set_facecolor(color)
        
    #plt.rc('font', size=2) 
    ax.set_xticks([1, 2, 3])
    ax.axes.get_xaxis().set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    #ax.set_xticklabels(['IMF-3', 'IMF-4', 'IMF-5']) 
    #plt.rc('font', size=8) 
    ax.set_ylabel('Frequency [Hz]') 
    ax.set_ylim([1, 128])
    
    if save:
        fname = 'Fig7_graph_abs_IF_hist' + str(c) + '.tiff'
        fig.savefig(fig_dir + fname, format='tiff', bbox_inches='tight',
                    pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
    
    IF[IF<0] = np.nan
    pvalue = np.zeros(3)*np.nan
    _ , pvalue[0] = stats.ttest_rel(IF[:, 2], IF[:, 3], nan_policy='omit')
    _ , pvalue[1] = stats.ttest_rel(IF[:, 2], IF[:, 4], nan_policy='omit')
    _ , pvalue[2] = stats.ttest_rel(IF[:, 3], IF[:, 4], nan_policy='omit')
    
    reject, corrpvalue, alphacSidak, alphacBonf = multipletests(pvalue, alpha=0.05, method='bonferroni')
    print('IF different with pvals: ', corrpvalue)

#%% Find mean IFs 
## Directory set-up
out_dir = './Data/'
fig_dir = './Figures/'
data_dir = '/media/marco/MSFabus_DPhil1/Datasets/itEMD/hc-3/ec013.30/ec013.454/ec013.454.eeg'

# Basic info and data loading
chans = 65
raw = np.fromfile(data_dir, dtype=np.int16).astype(float)
raw = raw.reshape(-1, chans)[:, 10]
data = raw[:1000*1250] / 1000
sample_rate = 1250
N_imf = 6
N_seg = 20

# Prepare data segmentation
ts = np.linspace(0, len(data)/sample_rate, len(data))
segments = np.linspace(0, 1000, N_seg+1)

# Prepare output
wfs = np.zeros((6, 48))
pmsi = np.zeros((3, N_seg))
N_w = np.zeros(3)
out = [[] for _ in range(len(segments)-1)]
imf_all = np.zeros((3, len(data), N_imf))*np.nan
mask_all = np.zeros((len(segments)-1, N_imf))*np.nan
niters_all = np.zeros(len(segments)-1)*np.nan
IF_seg = np.zeros((3, len(segments), 5))
IF_seg2 = np.zeros((3, len(segments), 5))
IF_seg3 = np.zeros((3, len(segments), 5))
print('\n Processing segment:')

ctr = 0
for s in range(len(segments)-1):
    print('\n %s / %s' %(s+1, len(segments)-1))
    
    # Select slice
    ROI = np.logical_and(ts > segments[s], ts < segments[s+1])
    x = data[ROI]
    time_vect = ts[ROI]
    
    for i in range(3):
        if i == 0:
        # itEMD
            [imf, mask_eq, mask_var, niters, _] = it_emd.it_emd(x, sample_rate=sample_rate,
                                                                   N_imf=6, N_avg=1,
                                                                   iter_th=0.1)
            out[s] = [imf, mask_eq, mask_var, time_vect, niters]
            N = imf.shape[1]
            mask_all[s, :N] = mask_eq
            niters_all[s] = niters   
        if i == 1:
            imf = emd.sift.mask_sift(x, mask_freqs='zc', max_imfs=N_imf)
        if i == 2:
            imf = emd.sift.ensemble_sift(x, max_imfs=N_imf)
        
        # Pick IMF closest to spectral peak
        IP, IF, IA = emd.spectra.frequency_transform(imf[:, :5], sample_rate, 'nht')
        IF_seg[i, s, :] = np.mean(IF, 0)
        IF_seg2[i, s, :] = np.median(IF, 0)
        IF_seg3[i, s, :] = np.average(IF, axis=0, weights=IA)
        
#%% Prepare figure
plt.rc('font', size=10) 
fig = plt.figure(figsize=(7.0625, 3.5))
axgrid = (4, 4)
spec = gridspec.GridSpec(ncols=2, nrows=2,
                         width_ratios=[1, 1], height_ratios=[0.25, 1])
ax0 = fig.add_subplot(spec[0, 0]) # Top: signal + IMF-4
ax1 = fig.add_subplot(spec[0, 1]) # Top: signal + IMF-4
ax2 = fig.add_subplot(spec[1, 0]) # HHT
ax3 = fig.add_subplot(spec[1, 1]) # Wavelet

plt.subplots_adjust(hspace=0.033, wspace=0.25)

# ax1.annotate('itEMD + HHT', xy=(0.36, 1.), fontsize=10, annotation_clip=False)
# ax1.annotate('Wavelet', xy=(0.314, -2.15), fontsize=10, annotation_clip=False)

# 7D, E, F: IMFs, HHTs    
ax = ax1
# Compute IMFs, IP, IF, IA
imf = imf_all[0, :, :]
IP, IF, IA = emd.spectra.frequency_transform(imf, sample_rate, 'nht')

# Top panel: IMFs
# utils.plot_imfs(imf[14*sample_rate:16*sample_rate, :5], sample_rate, fig,
#                 ax, pos=[0, 0.64, 0.212, 0.16], fontsize=4, nticks=4,
#                 lw=0.75, labelpad=3, xlab=False)
sig = np.sum(imf[14*sample_rate:16*sample_rate, :5], axis=1)
tits = ['itEMD + HHT', 'Wavelet']
for c, ax in enumerate([ax0, ax1]):
    ax.plot(sig, lw=1, label='Rat LFP')
    ax.plot(imf[14*sample_rate:16*sample_rate, 3], lw=1.25, label='IMF-4')
    ax.set_xlim(0, 2*sample_rate)
    ax.set_title(tits[c], fontsize=14)
    ax.axis('off')

leg = ax.legend(ncol=1, bbox_to_anchor=(1.45, 1.0), loc='upper right',
                fontsize=8)
for line in leg.get_lines():
    line.set_linewidth(4.0)
# # Bottom panel: HHT
# ax = ax2
# freq_edges, freq_centres = emd.spectra.define_hist_bins(1, 65, 96, 'log')
# ixh = list(range(14*sample_rate, 16*sample_rate))
# hht = emd.spectra.hilberthuang(IF, IA, freq_edges, mode='amplitude')[:, ixh]
# hht = ndimage.gaussian_filter(hht, 0.5)
# time_v = np.arange(hht.shape[1] - 0.5) / 1250

# pcm = emd.plotting.plot_hilberthuang(hht, time_v, freq_centres,
#                            cmap='viridis', time_lims=(0, 2),  log_y=True, 
#                            vmin=0, vmax=0.5, fig=fig, ax=ax2,
#                            return_pcm=True)
# ax.minorticks_off()
# ax.set_xticks([0, 0.5, 1, 1.5, 2])
# ax.set_xticklabels(['0', '', '1', '', '2'])
# ax.set_yticks([1, 2, 4, 8, 16, 32, 64])
# ax.set_xlabel('Time [s]', fontsize=10, labelpad=3)
# ax.set_ylabel('Frequency [Hz]')
    
# cax = fig.add_axes([0.94, 0.123, 0.02, 0.58])
# cbar = fig.colorbar(pcm, cax=cax)
# cbar.set_ticks([0, 0.1, 0.2, 0.3, 0.4, 0.5])
# cbar.ax.tick_params(labelsize=10)
# cbar.set_label('Amplitude', fontsize=10)


# # 7G: STFT / Wavelet
# ax10 = ax3
# y = np.sum(imf_all[0, 14*sample_rate:16*sample_rate, :], axis=1)
# w = 4.
# ftf = np.linspace(1, 64, 100)
# widths = w*sample_rate / (2*ftf*np.pi)
# cwtm = signal.cwt(y, signal.morlet2, widths, w=w)
# ftt = np.linspace(14, 16, cwtm.shape[1])
# ax10.pcolormesh(ftt, ftf, np.abs(cwtm), cmap='viridis', shading='nearest')
# ax10.set_yscale('log')
# ax10.minorticks_off()
# ax10.set_ylim(1, 64)
# ax10.set_xticks([14, 14.5, 15, 15.5, 16])
# ax10.set_xticklabels(['0', '', '1', '', '2'])
# ax10.set_yticks([1, 2, 4, 8, 16, 32, 64])
# ax10.set_yticklabels(['1', '2', '4', '8', '16', '32', '64'])
# ax10.set_xlabel('Time [s]', fontsize=10, labelpad=3)
# ax10.set_ylabel('Frequency [Hz]')

# if save:
#     fname = 'Fig7_graph_abs.tiff'
#     fig.savefig(fig_dir + fname, format='tiff', bbox_inches='tight',
#                 pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
    
# plt.show()
