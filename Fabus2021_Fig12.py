#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script plots Figures 1 and 2 in Fabus et al (2021).

@author: MSFabus
"""
import matplotlib.pyplot as plt
import numpy as np
import emd
from Tools import it_emd, sim, utils

## Plotting options
save = True
plt.rcParams['figure.dpi'] = 400
dpi = 600
data_dir = './Data_prebaked/Simulations/'
fig_dir = './Figures/'
        
#%% Prepare figure
plt.rc('font', size=12)
fig, axs = plt.subplots(2, 2, gridspec_kw={'width_ratios': [1., 1]},
                        figsize=(7.0625, 5))

# Figure 1
# 1A: EMD with no noise or bursts
ax1 = axs.flatten()[0]
ax1.annotate('A', xy=(-0.6, 1.45), fontsize=32, annotation_clip=False)

f0 = 4
sim_ = sim.sim(t=2, sample_rate=512)
x1 = sim_.sim_sig(N_comp=1, amps=[1], f=[f0], shape=['it_sin'], 
                  noise=False, amp_mod=[1], it_sin_order=8)
imf = emd.sift.sift(sim_.x.T, max_imfs=2)

utils.plot_imfs(imf[:512, :], 512, fig, ax1, pos=[0, 0.9, 0.4, 0.4])

# 1B,C: EMD with noise and a burst
x = utils.simulate_fig12_burst()
ax2 = axs.flatten()[1]
ax3 = axs.flatten()[2]
ax1.annotate('B', xy=(0.85, 1.45), fontsize=32, annotation_clip=False)
ax1.annotate('C', xy=(-0.6, -0.17), fontsize=32, annotation_clip=False)

for k in range(2): #Classic, Dyadic
    if k == 0:
        imf = emd.sift.sift(x, max_imfs=6)
        utils.plot_imfs(imf[:512, :4], sim_.sample_rate, fig, ax2, pos=[0.5, 0.95, 0.4, 0.4])

    if k == 1:
        mask = np.array([128, 30, 4, 0.01])
        imf = emd.sift.mask_sift(x, max_imfs=4, mask_freqs=mask/sim_.sample_rate)
        utils.plot_imfs(imf[:512, :], sim_.sample_rate, fig, ax3, pos=[0, 0.38, 0.4, 0.4])
                
# 1D
data = np.load(data_dir + 'itEMD_Fig1_data.npz')
ph = data['phases']
IFgt = data['IFgt']
IFemd = data['IFemd']
N = IFemd.shape[0]

cols = ['purple', 'darkorange', 'cornflowerblue']
labs = ['Standard Sift', 'Dyadic Mask Sift', 'Ensemble Sift']

ax = axs.flatten()[3]
ax1.annotate('D', xy=(0.9, -0.35), fontsize=32, annotation_clip=False)
ax.plot(ph, IFgt, label='Ground truth', linewidth=3, color='k')

for k in range(3):
    if_m = np.mean(IFemd[:, k, :], axis=0)
    er = np.std(IFemd[:, k, :], axis=0) / (np.sqrt(N))
    ax.plot(ph, if_m, label=labs[k], linewidth=3, color=cols[k])
    ax.fill_between(ph, if_m-er, if_m+er, alpha=0.5, color=cols[k])

ax.set_xlabel('Phase [rad]')
ax.set_ylabel('IF [Hz]')
leg = ax.legend(ncol=2, bbox_to_anchor=(1.05, -0.28), loc='upper right')
for line in leg.get_lines():
    line.set_linewidth(3.0)
plt.rc('font', size=4)
ax.set_xticks([0, np.pi/2, np.pi, 3*np.pi/2, 2*np.pi])
plt.rc('text', usetex=True)
ax.set_xticklabels([0, '$\pi /2$', '$\pi$', '$3 \pi /2$', '$2 \pi$'])
plt.rc('text', usetex=False)
ax.set_ylim([2.2, 7.2])
ax.set_yticks([3, 4, 5, 6, 7])
ax.grid(True)
ax.set_axisbelow(True) 
if save:
    fig.savefig(fig_dir + 'Fig1.tiff', format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()



#%% Figure 2: Random mask initialisation
# Prepare figure
plt.rc('font', size=12)
fig, axs = plt.subplots(1, 2,      gridspec_kw={'width_ratios': [1., 1]},
                                   figsize=(7.0625, 3))
# 2A
ax1 = axs[0]
ax1.annotate('A', xy=(-0.6, 1.2), fontsize=32, annotation_clip=False)

Fs = 512
x = utils.simulate_fig12_burst()
np.random.seed(6)
m0 = np.random.randint(1, 128, size=6) / Fs

nit, m_all, imf, mask_eq, mask_std, imf_all = it_emd.it_emd(x, N_imf=6, 
                                                      N_iter_max=15, N_avg=1, 
                                                  sample_rate=Fs,
                                      verbose=True, mask_0=m0,
                                      iter_th=0.01, w_method='IA')
utils.plot_imfs(imf[:512, :4], 512, fig, ax1, pos=[0, 0.87, 0.43, 0.8], fontsize=10)

# 2B
ax = axs[1]
ax1.annotate('B', xy=(1, 1.18), fontsize=32, annotation_clip=False)
data = np.load(data_dir + 'itEMD_Fig2_data.npz')
itr = data['itr']
mask_all = data['mask']
mask_all_avg = np.nanmedian(mask_all, axis=0)
w = data['w']
N = mask_all.shape[0]

for j in range(6):
    for k in range(N):
        ax.plot(itr, mask_all[k, j, :]*Fs, color='grey', alpha=0.2, lw=0.5)
for j in range(4):
    fr = Fs*mask_all_avg[j, :]
    ax.plot(itr, fr, label='IMF-{0}'.format(j+1), 
             linewidth=w[j]*3)
ax.set_yscale('log')
ax.grid(True)
ax.set_ylabel('IF [Hz]')
ax.set_xlabel('Iteration')
plt.rc('font', size=12) 
ax.legend(ncol=2, bbox_to_anchor=(1, 1.3), loc='upper right')
ax.set_xlim([-0., 15])
ax.set_xticks([0, 5, 10, 15])
ax.set_ylim([1, 256])
if save:
    fig.savefig(fig_dir + 'Fig2.tiff', format='tiff', 
                bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()