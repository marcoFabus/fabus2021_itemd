#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script plots simulated data results (Figures 3-6) from saved files.

@author: MSFabus
"""
import matplotlib.pyplot as plt
import numpy as np
from statsmodels.sandbox.stats.multicomp import multipletests
from scipy import stats
import emd
from Tools import it_emd, sim, utils

## Plotting options
save = True
plt.rcParams['figure.dpi'] = 200
dpi = 600
data_dir = './Data_prebaked/Simulations/'
fig_dir = './Figures/'

#Labels
cols = ['darkorange', 'cornflowerblue', 'green']
labs = ['Dyadic Mask Sift', 'Ensemble Sift', 'itEMD']


#%% Figure 3
# Prepare figure
plt.rc('font', size=12)
fig, axs = plt.subplots(2, 2,      gridspec_kw={'width_ratios': [1., 1]},
                                   figsize=(7.0625, 5))
plt.subplots_adjust(hspace=0.6)
## 3A: Pearson r figure
ax = axs.flatten()[0]
ax.annotate('A', xy=(-0.9, 1.45), fontsize=24, annotation_clip=False)
ax.annotate('B', xy=(3.35, 1.45), fontsize=24, annotation_clip=False)
ax.annotate('C', xy=(-0.9, -2), fontsize=24, annotation_clip=False)
ax.annotate('D', xy=(3.35, -2), fontsize=24, annotation_clip=False)

data = np.load(data_dir + 'itEMD_Fig3_data.npz')
noise = data['noise']
corr = data['corr']
[corr_mean, corr_er, H0_rej] = utils.prepare_fig(corr, H1=2, H0=[0,1], comparison='best',
                                           contrast='higher', smoothing_win=3,
                                           p_lim=0.01)

# Find regions with no convergence
failed_iter = data['noise_flag'][np.sum(data['flag'], 0)/30 > 0.15].max()

#Plot correlation figure
for l in range(3):
    ax.plot(noise, corr_mean[l, :], label=labs[l], color=cols[l])
    ax.fill_between(noise,  corr_mean[l, :] - corr_er[l, :], 
                    corr_mean[l, :] + corr_er[l, :], alpha=0.5, color=cols[l])
ax.plot(noise, H0_rej*1, color='k', linewidth=2)
ax.set_ylabel('Pearson r')
ax.set_xlabel('RMS Noise')
ax.grid(True)
ax.set_xlim(0, 3)
ax.set_ylim(-1.1, 1.1)
ax.set_xticks([0, 1, 2, 3])
ax.set_yticks([-1, -0.5, 0, 0.5, 1])
ax.tick_params(axis='x', pad=5)
leg = ax.legend(ncol=3, bbox_to_anchor=(1.07, 1.25), loc='upper right',
                fontsize=7)
for line in leg.get_lines():
    line.set_linewidth(6.0)
axx = ax.add_artist(leg)

failed = ax.fill_between([0, failed_iter], [-1.1, -1.1], [1.1, 1.1], 
                         color='firebrick', alpha=0.35)

## 3B, D: Example simulations
sim_ = sim.sim(t=10, sample_rate=512)
x = sim_.sim_sig(N_comp=1, amps=[1], f=[4], shape=['it_sin'], 
             noise=True, amp_mod=[1], it_sin_order=8,
             noise_type='white', a_noise=0.5, seed=0)

axxs = [axs.flatten()[1], axs.flatten()[3]]
poss = [[0.6, 0.4, 0.4, 0.3], [0.6, 0.85, 0.4, 0.3]]
for k in [0, 1]: # Dyadic, itEMD
    if k == 0:
        imf = emd.sift.mask_sift(x, max_imfs=6, mask_freqs='zc')
    if k == 1:
        imf, mask_eq, mask_std, niter, flag = it_emd.it_emd(x, N_imf=6, 
                                  N_iter_max=15, N_avg=1, 
                                  iter_th=0.1, sample_rate=512,
                                  mask_0='zc', ignore_last=True) 
    ax = axxs[k]
    utils.plot_imfs(imf[512:6*512, :5], sim_.sample_rate, fig, ax, 
                    pos=poss[k], fontsize=7, nticks=5)
    
## 3C: PMSI figure
pmsi = data['pmsi']
[pmsi_mean, pmsi_er, H0_rej] = utils.prepare_fig(pmsi, comparison='best', 
                                           contrast='lower', H1=2, H0=[0,1])

ax = axs.flatten()[2]

for l in range(3):
    ax.plot(noise, pmsi_mean[l, :], label=labs[l], color=cols[l])
    ax.fill_between(noise,  pmsi_mean[l, :] - pmsi_er[l, :], 
                    pmsi_mean[l, :] + pmsi_er[l, :], alpha=0.5, color=cols[l])
ax.plot(noise, H0_rej*0.4, color='k', linewidth=2)
ax.set_ylabel('PMSI')
ax.set_xlabel('RMS Noise')
ax.grid(True)
ax.set_xlim(0, 3)
ax.set_ylim(-0.01, 0.41)
ax.set_xticks([0, 1, 2, 3])
ax.set_yticks([0, 0.2, 0.4])
ax.tick_params(axis='x', pad=5)
leg = ax.legend(ncol=3, bbox_to_anchor=(1.07, 1.2), loc='upper right',
                fontsize=7)
for line in leg.get_lines():
    line.set_linewidth(6.0)
axx = ax.add_artist(leg)

failed = ax.fill_between([0, failed_iter], [-0.01, -0.01], [0.41, 0.41], 
                         color='firebrick', alpha=0.35)
if save:
    fig.savefig(fig_dir + 'Fig3.tiff', format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()


#%% Figure 4
# Prepare figure
plt.rc('font', size=12)
fig, axs = plt.subplots(2, 2,      gridspec_kw={'width_ratios': [1., 1]},
                                   figsize=(7.0625, 5))
plt.subplots_adjust(hspace=0.6)
ax = axs.flatten()[0]
ax.annotate('A', xy=(-15, 1.25), fontsize=24, annotation_clip=False)
ax.annotate('B', xy=(115, 1.25), fontsize=24, annotation_clip=False)
ax.annotate('C', xy=(-15, -0.8), fontsize=24, annotation_clip=False)
ax.annotate('D', xy=(115, -0.8), fontsize=24, annotation_clip=False)

## 4A: Pearson r figure
data = np.load(data_dir + 'itEMD_Fig4_data.npz')
fd = data['freq_dist']
corr = data['corr']
[corr_mean, corr_er, H0_rej] = utils.prepare_fig(corr, H1=2, H0=[0,1], comparison='best',
                                           contrast='higher', smoothing_win=3,
                                           p_lim=0.01)

for l in range(3):
    ax.plot(fd, corr_mean[l, :], label=labs[l], color=cols[l])
    ax.scatter(fd, corr_mean[l, :], color=cols[l], s=10)
    ax.fill_between(fd,  corr_mean[l, :] - corr_er[l, :], 
                    corr_mean[l, :] + corr_er[l, :], alpha=0.5, color=cols[l])
ax.plot(fd, H0_rej*1, color='k', linewidth=2)
ax.set_ylabel('Pearson r')
ax.set_xlabel('Frequency Distortion [%]')
ax.grid(True)
ax.set_ylim(-0.25, 1.1)
ax.set_yticks([0, 0.5, 1])
ax.set_xticks([20, 40, 60, 80, 100])
ax.tick_params(axis='x', pad=5)

leg = ax.legend(ncol=3, bbox_to_anchor=(1.07, 1.2), loc='upper right',
                fontsize=7)
for line in leg.get_lines():
    line.set_linewidth(6.0)

## 4B, D: Example simulations
sim_ = sim.sim(t=10, sample_rate=512)
x = sim_.sim_sig(N_comp=1, amps=[1], f=[4], shape=['it_sin'], 
             noise=True, amp_mod=[1], it_sin_order=12,
             noise_type='white', a_noise=1, seed=0)

axxs = [axs.flatten()[1], axs.flatten()[3]]
poss = [[0.6, 0.4, 0.4, 0.3], [0.6, 0.85, 0.4, 0.3]]
for k in [0, 1]: # Dyadic, itEMD
    if k == 0:
        imf = emd.sift.mask_sift(x, max_imfs=6, mask_freqs='zc')
    if k == 1:
        imf, mask_eq, mask_std, niter, flag = it_emd.it_emd(x, N_imf=6, 
                                  N_iter_max=15, N_avg=1, 
                                  iter_th=0.1, sample_rate=512,
                                  mask_0='zc', ignore_last=True) 
    ax = axxs[k]
    utils.plot_imfs(imf[512:6*512, :5], sim_.sample_rate, fig, ax, 
                    pos=poss[k], fontsize=7, nticks=5)
    
## 4C: PMSI figure
ax = axs.flatten()[2]
pmsi = data['pmsi']
[pmsi_mean, pmsi_er, H0_rej] = utils.prepare_fig(pmsi, comparison='best', 
                                           contrast='lower', H1=2, H0=[0,1])

for l in range(3):
    ax.plot(fd, pmsi_mean[l, :], label=labs[l], color=cols[l])
    ax.scatter(fd, pmsi_mean[l, :], color=cols[l], s=10)
    ax.fill_between(fd,  pmsi_mean[l, :] - pmsi_er[l, :], 
                    pmsi_mean[l, :] + pmsi_er[l, :], alpha=0.5, color=cols[l])
ax.plot(fd, H0_rej*0.4, color='k', linewidth=2)
ax.set_ylabel('PMSI')
ax.set_xlabel('Frequency Distortion [%]')
ax.grid(True)
ax.set_yticks([0, 0.2, 0.4])
ax.set_xticks([20, 40, 60, 80, 100])
ax.tick_params(axis='x', pad=5)
plt.rc('font', size=20)
leg = ax.legend(ncol=3, bbox_to_anchor=(1.07, 1.2), loc='upper right',
                fontsize=7)
for line in leg.get_lines():
    line.set_linewidth(6.0)
    
if save:
    fig.savefig(fig_dir + 'Fig4.tiff', format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()


#%% Figure 5
# Prepare figure
plt.rc('font', size=12)
fig, axs = plt.subplots(2, 2,      gridspec_kw={'width_ratios': [1., 1]},
                                   figsize=(7.0625, 5))
plt.subplots_adjust(hspace=0.2)
ax = axs.flatten()[0]
ax.annotate('A', xy=(-0.65, 1.25), fontsize=24, annotation_clip=False)
ax.annotate('B', xy=(1, 1.25), fontsize=24, annotation_clip=False)
ax.annotate('C', xy=(-0.65, -0.29), fontsize=24, annotation_clip=False)
ax.annotate('D', xy=(1, -0.29), fontsize=24, annotation_clip=False)
ax.annotate('itEMD', xy=(0.15, 1.28), fontsize=16, annotation_clip=False)
ax.annotate('Dyadic Mask', xy=(1.6, 1.28), fontsize=16, annotation_clip=False)
ax.annotate('EEMD', xy=(0.16, -0.25), fontsize=16, annotation_clip=False)

ax3 = axs.flatten()[3]
ax3.axis('off')

## 5A, B, C: Example simulations
sim_ = sim.sim(t=20, sample_rate=512)
x = sim_.sim_sig(N_comp=1, amps=[1], f=[4], shape=['it_sin'], 
             noise=True, amp_mod=[1], it_sin_order=4,
             noise_type='white', a_noise=0.1, seed=0)

axxs = [axs.flatten()[0], axs.flatten()[1], axs.flatten()[2]]
poss = [[0., 0.38, 0.45, 0.35], [0., 0.9, 0.45, 0.35], [0.55, 0.9, 0.45, 0.35]]
xlabs=[True, False, False]
for k in [0, 1, 2]: # Dyadic, itEMD
    if k == 2:
        imf = emd.sift.mask_sift(x, max_imfs=6, mask_freqs='zc',
                                 mask_amp_mode='ratio_sig')
    if k == 0:
        imf = emd.sift.ensemble_sift(x, max_imfs=6)
    if k == 1:
        imf, mask_eq, mask_std, niter, flag = it_emd.it_emd(x, N_imf=6, 
                                  N_iter_max=15, N_avg=1, 
                                  iter_th=0.1, sample_rate=512,
                                  mask_0='zc', ignore_last=True) 
    ax = axxs[k]
    utils.plot_imfs(imf[17*512:19*512, :5], sim_.sample_rate, fig, ax, 
                    pos=poss[k], fontsize=7.5, nticks=5, xlab=xlabs[k])

## Waveform Shape / IF Analysis
#Labels
cols = ['darkorange', 'cornflowerblue', 'green']
labs = ['Dyadic Mask Sift', 'Ensemble Sift', 'itEMD']

data = np.load(data_dir + 'itEMD_Fig5_data.npz')
ph = data['phases']
IFgt = data['IFgt']
IFemd = data['IFemd']
IFstd = data['IFstd']
N = data['N']

# Bottom: Plot IFs
ax = fig.add_axes([0.55, 0., 0.45, 0.2])
ax.plot(ph, IFgt, label='Ground truth', linewidth=2, color='k')

for k in range(3):
    if_m = IFemd[k, :]
    er = IFstd[k, :] / np.sqrt(N[k]) 
    ax.plot(ph, if_m, label=labs[k], linewidth=2, color=cols[k])
    ax.fill_between(ph, if_m-er, if_m+er, alpha=0.5, color=cols[k])

ax.set_xlabel('Phase [rad]', fontsize=12)
ax.set_ylabel('IF [Hz]', labelpad=6, fontsize=12)
ax.set_xticks([0, np.pi/2, np.pi, 3*np.pi/2, 2*np.pi])
plt.rc('text', usetex=True)
ax.set_xticklabels([0, '$\pi /2$', '$\pi$', '$3 \pi /2$', '$2 \pi$'],
                   fontsize=12)
plt.rc('text', usetex=False)
ax.set_yticks([3, 4, 5])
ax.set_yticklabels([3, 4, 5], fontsize=12)
ax.set_ylim(3, 5.2)
ax.grid(True)
ax.set_axisbelow(True) 

# Top: Plot waveforms
ax2 = fig.add_axes([0.55, 0.22, 0.45, 0.2])
avgf_gt = np.mean(IFgt)
IPgt = np.cumsum(2 * np.pi * IFgt / (48*avgf_gt))
waveform_gt = np.sin(IPgt)

avgf_emd = np.mean(IFemd, axis=1)
IPemd = np.zeros((3, 48))
waveform_emd = np.zeros((3, 48))

for k in range(3):
    IPemd[k, :] = np.cumsum(2 * np.pi * IFemd[k, :] / (48*avgf_emd[k]))
    waveform_emd[k, :] = np.sin(IPemd[k, :])

for k in range(3):
    if_m = waveform_emd[k, :]
    ax2.plot(ph, if_m, label=labs[k], linewidth=2, color=cols[k])
ax2.plot(ph, waveform_gt, label='Ground truth', linewidth=1, color='k')
ax2.set_ylabel('Signal', labelpad=3, fontsize=12)
leg = ax2.legend(ncol=2, bbox_to_anchor=(0.9, 1.42), loc='upper right', 
           fontsize=7)
for line in leg.get_lines():
    line.set_linewidth(3.0)
ax2.set_xticks([0, np.pi/2, np.pi, 3*np.pi/2, 2*np.pi])
ax2.set_xticklabels([])
ax2.set_yticks([-1, 0, 1])
ax2.set_yticklabels([-1, 0, 1], fontsize=12)
ax2.set_ylim(-1.1, 1.1)
ax2.grid(True)

if save:
    fig.savefig(fig_dir + 'Fig5.tiff', format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()

# Stats
rmse = data['rmse'].T
pmsi = data['pmsi']
pvalue = np.zeros(3)*np.nan
_ , pvalue[0] = stats.ttest_ind(rmse[:, 0], rmse[:, 1], nan_policy='omit', equal_var=False)
_ , pvalue[1] = stats.ttest_ind(rmse[:, 0], rmse[:, 2], nan_policy='omit', equal_var=False)
_ , pvalue[2] = stats.ttest_ind(rmse[:, 1], rmse[:, 2], nan_policy='omit', equal_var=False)

reject, corrpvalue, alphacSidak, alphacBonf = multipletests(pvalue, alpha=0.05, method='bonferroni')


#%% Figure 6
# Prepare figure
plt.rc('font', size=12)
fig, axs = plt.subplots(2, 2,      gridspec_kw={'width_ratios': [1., 1]},
                                   figsize=(7.0625, 5))
plt.subplots_adjust(hspace=0.6)
ax = axs.flatten()[0]
ax.annotate('A', xy=(-30, 1.25), fontsize=24, annotation_clip=False)
ax.annotate('B', xy=(110, 1.25), fontsize=24, annotation_clip=False)
ax.annotate('C', xy=(-30, -0.8), fontsize=24, annotation_clip=False)
ax.annotate('D', xy=(110, -0.8), fontsize=24, annotation_clip=False)

## 6A: Pearson r figure
data = np.load(data_dir + 'itEMD_Fig6_data.npz')
num_osc = data['num_osc']
corr = data['corr']
[corr_mean, corr_er, H0_rej] = utils.prepare_fig(corr, H1=2, H0=[0,1], comparison='best',
                                           contrast='higher', smoothing_win=3)

#Plot correlation figure
for l in range(3):
    ax.plot(num_osc, corr_mean[l, :], label=labs[l], color=cols[l])
    ax.fill_between(num_osc,  corr_mean[l, :] - corr_er[l, :], 
                    corr_mean[l, :] + corr_er[l, :], alpha=0.5, color=cols[l])
ax.plot(num_osc, H0_rej*1, color='k', linewidth=2)
ax.set_ylabel('Pearson r')
ax.set_xlabel('Number of Cycles in Burst')
ax.grid(True)
ax.set_ylim(-0.25, 1.1)
ax.set_yticks([0, 0.5, 1])
ax.set_xlim(0, 100)
ax.tick_params(axis='x', pad=5)
leg = ax.legend(ncol=3, bbox_to_anchor=(1.07, 1.2), loc='upper right',
                fontsize=7)
for line in leg.get_lines():
    line.set_linewidth(6.0)

## 6B, D: Example simulations
a = np.zeros(25*512)
idx = list(range(int(3*512), int(5.5*512)))
a[idx] = 1 
sim_ = sim.sim(t=25, sample_rate=512)
x = sim_.sim_sig(N_comp=1, amps=[1], f=[4], shape=['it_sin'], 
             noise=True, amp_mod=[a], it_sin_order=8,
             noise_type='white', a_noise=1, seed=0)

axxs = [axs.flatten()[1], axs.flatten()[3]]
poss = [[0.6, 0.4, 0.4, 0.3], [0.6, 0.85, 0.4, 0.3]]
for k in [0, 1]: # Dyadic, itEMD
    if k == 0:
        imf = emd.sift.mask_sift(x, max_imfs=6, mask_freqs='zc')
    if k == 1:
        imf, mask_eq, mask_std, niter, flag = it_emd.it_emd(x, N_imf=6, 
                                  N_iter_max=15, N_avg=1, 
                                  iter_th=0.1, sample_rate=512,
                                  mask_0='zc', ignore_last=True) 
    ax = axxs[k]
    utils.plot_imfs(imf[:20*512, :5], sim_.sample_rate, fig, ax, 
                    pos=poss[k], fontsize=7, nticks=5)
    
## 6C: PMSI figure
ax = axs.flatten()[2]
pmsi = data['pmsi']
[pmsi_mean, pmsi_er, H0_rej] = utils.prepare_fig(pmsi, comparison='best', 
                                           contrast='lower', H1=2, H0=[0,1])

for l in range(3):
    ax.plot(num_osc, pmsi_mean[l, :], label=labs[l], color=cols[l])
    ax.fill_between(num_osc,  pmsi_mean[l, :] - pmsi_er[l, :], 
                    pmsi_mean[l, :] + pmsi_er[l, :], alpha=0.5, color=cols[l])
ax.plot(num_osc, H0_rej*0.4, color='k', linewidth=2)
ax.set_ylabel('PMSI')
ax.set_xlabel('Number of Cycles in Burst')
ax.grid(True)
ax.set_yticks([0, 0.2, 0.4])
ax.set_xlim(0, 100)
ax.tick_params(axis='x', pad=5)
leg = ax.legend(ncol=3, bbox_to_anchor=(1.07, 1.2), loc='upper right',
                fontsize=7)
for line in leg.get_lines():
    line.set_linewidth(6.0)
if save:
    fig.savefig(fig_dir + 'Fig6.tiff', format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()