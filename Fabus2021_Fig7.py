import emd
import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage, stats, signal
from statsmodels.sandbox.stats.multicomp import multipletests
from Tools import it_emd, utils
from matplotlib import gridspec, artist, spines
from mpl_toolkits.axes_grid1.inset_locator import (inset_axes, InsetPosition,
                                                  mark_inset)
from matplotlib.colors import ListedColormap, LinearSegmentedColormap, to_rgba_array

## Plotting options
save = True
plt.rcParams['figure.dpi'] = 200
dpi = 600
data_dir = './Data_prebaked/LFP/'
fig_dir = './Figures/'

#Labels
labs = ['itEMD','Dyadic', 'Ensemble']
cols = ['green','darkorange', 'cornflowerblue']
panels = ['D', 'E', 'F']

# Load data
data = np.load(data_dir + 'itEMD_Fig7_data.npz')
pmsi = data['pmsi'].T
imf_all = data['imf']
wfs = data['wfs']
N_w = data['N_w']
niters = data['niters']
ph = data['ph']
sample_rate = 1250

# Print niters info
print('hc3 # of iterations =  %.0f +/- %.0f' % (np.mean(niters), np.std(niters)))

#%% Prepare figure
plt.rc('font', size=7) 
fig = plt.figure(figsize=(7.0625, 7.0625))
axgrid = (4, 4)
spec = gridspec.GridSpec(ncols=3, nrows=4,
                         width_ratios=[1, 1, 1])
ax1 = fig.add_subplot(spec[0, 0])
ax2 = fig.add_subplot(spec[0, 1])
ax3 = fig.add_subplot(spec[0, 2])
ax4 = fig.add_subplot(spec[1, 0])
ax5 = fig.add_subplot(spec[1, 1])
ax6 = fig.add_subplot(spec[1, 2])
ax7 = fig.add_subplot(spec[2, 0])
ax8 = fig.add_subplot(spec[2, 1])
ax9 = fig.add_subplot(spec[2, 2])
ax10 = fig.add_subplot(spec[3, 0])
plt.subplots_adjust(hspace=0.6, wspace=0.33)
ax4.annotate('A', xy=(-0.25, 2.75), fontsize=18, annotation_clip=False)
ax4.annotate('B', xy=(1.1, 2.75), fontsize=18, annotation_clip=False)
ax4.annotate('C', xy=(2.4, 2.75), fontsize=18, annotation_clip=False)
ax4.annotate('D', xy=(-0.25, 1.), fontsize=18, annotation_clip=False)
ax4.annotate('E', xy=(1.1, 1.), fontsize=18, annotation_clip=False)
ax4.annotate('F', xy=(2.4, 1.), fontsize=18, annotation_clip=False)
ax4.annotate('G', xy=(-0.25, -2.1), fontsize=18, annotation_clip=False)
ax4.annotate('H', xy=(1.1, -2.1), fontsize=18, annotation_clip=False)
ax4.annotate('I', xy=(2.55, -2.1), fontsize=18, annotation_clip=False)

ax4.annotate('itEMD', xy=(0.36, 1.), fontsize=10, annotation_clip=False)
ax4.annotate('Dyadic', xy=(1.67, 1.), fontsize=10, annotation_clip=False)
ax4.annotate('EEMD', xy=(3.03, 1.), fontsize=10, annotation_clip=False)
ax4.annotate('***', xy=(3.1, 2.6), fontsize=8, annotation_clip=False)
ax4.annotate('***', xy=(2.93, 2.448), fontsize=8, annotation_clip=False)
ax4.annotate('***', xy=(3.27, 2.415), fontsize=8, annotation_clip=False)
ax4.annotate('Wavelet', xy=(0.314, -2.15), fontsize=10, annotation_clip=False)

# 7A: Power Spectrum
# Load data - needs to point to hc-3 .eeg dataset! 
f = '/media/marco/MSFabus_DPhil1/Datasets/itEMD/hc-3/ec013.30/ec013.454/ec013.454.eeg'
chan = 65  
raw = np.fromfile(f, dtype=np.int16).astype(float)
raw = raw.reshape(-1,chan)[:,10]
data = raw[:1000*1250] / 1000

#plt.rc('font', size=36)
freqs, psd = utils.plot_psd(data, 1250, return_psd=True)

ax1.semilogy(freqs,psd, color='k', lw=1)
ax1.set_xscale('log')
ax1.set_xlim(1, 100)
ax1.set_ylim(1e-5, 0.1)
ax1.set_yticks([1e-5, 1e-4, 1e-3, 1e-2, 0.1])
ax1.set_xlabel('Frequency [Hz]')
ax1.set_ylabel('Power Spectrum [m$V^2$]')
ax1.grid(True)
ax1.set_axisbelow(True) 


# 7B: Average waveshapes
ax = ax2
for k in range(3):
    if_m = wfs[k, :]
    er = wfs[k+3, :] / np.sqrt(N_w[k])
    ax.plot(ph, if_m, label=labs[k], linewidth=1, color=cols[k])
    ax.fill_between(ph, if_m-er, if_m+er, alpha=0.5, color=cols[k])
ax.set_xlabel('Phase [rad]')
ax.set_ylabel('IF [Hz]')
leg = ax.legend(ncol=1, loc=2, fontsize=5)
for line in leg.get_lines():
    line.set_linewidth(4.0)
ax.set_xticks([0, np.pi/2, np.pi, 3*np.pi/2, 2*np.pi])
plt.rc('text', usetex=True)
ax.set_xticklabels([0, '$\pi /2$', '$\pi$', '$3 \pi /2$', '$2 \pi$'])
plt.rc('text', usetex=False)
ax.set_yticks([7, 8, 9])
ax.set_xlim(0, 2*np.pi)
ax.set_ylim(7, 9)
ax.tick_params(axis='x', pad=5)
ax.grid(True)
ax.set_axisbelow(True) 
    
# 7C: PMSI violin plots
ax = ax3
violin_parts = ax.violinplot(pmsi, showextrema=False, showmedians=True, widths=0.7)
ct = 0
for pc in violin_parts['bodies']:
    pc.set_facecolor(cols[ct])
    pc.set_edgecolor('black')
    pc.set_alpha(1)
    ct += 1

violin_parts['cmedians'].set_edgecolor('black')
violin_parts['cmedians'].set_linewidth(1.5)

ax.plot([1, 2], [0.36, 0.36], color='k', lw=2)
ax.plot([1, 3], [0.4, 0.4], color='k', lw=2)
ax.plot([2, 3], [0.34, 0.34], color='k', lw=2)

ax.set_ylabel('PMSI')
ax.set_xticks([1, 2, 3])
ax.set_yticks([0, 0.1, 0.2, 0.3, 0.4])
ax.set_xticklabels(['itEMD', 'Dyadic', 'EEMD'])
ax.tick_params(axis='x', pad=5)

# Stats
pvalue = np.zeros(3)*np.nan
_ , pvalue[0] = stats.ttest_rel(pmsi[:, 0], pmsi[:, 1], nan_policy='omit')
_ , pvalue[1] = stats.ttest_rel(pmsi[:, 0], pmsi[:, 2], nan_policy='omit')
_ , pvalue[2] = stats.ttest_rel(pmsi[:, 1], pmsi[:, 2], nan_policy='omit')

pvalue *= 2 #one-tailed result
reject, corrpvalue, alphacSidak, alphacBonf = multipletests(pvalue, alpha=0.05, method='bonferroni')
print('PMSI lower with pvals: ', corrpvalue)

# Custom colormap
viridis = plt.cm.get_cmap('hot', 256)
newcolors = viridis(np.linspace(0, 1, 256))
bcg = np.array([208, 250, 250, 256])/256
newcolors[:10, :] = to_rgba_array('lightskyblue')
newcmp = ListedColormap(newcolors)
newcmp = viridis
# 7D, E, F: IMFs, HHTs
aximf = [ax4, ax5, ax6]
axhht = [ax7, ax8, ax9]
poss = [[0.125], [0.405], [0.687]]
inset_pos = [0, 0.45, 0.73]
for i in range(3):
    
    # Compute IMFs, IP, IF, IA
    imf = imf_all[i, :, :]
    IP, IF, IA = emd.spectra.frequency_transform(imf, sample_rate, 'nht')
    
    # Top panel: IMFs
    utils.plot_imfs(imf[14*sample_rate:16*sample_rate, :5], sample_rate, fig,
                    aximf[i], pos=poss[i] + [0.64, 0.212, 0.16], fontsize=4, nticks=4,
                    lw=0.75, labelpad=3, xlab=False)
    
    # Bottom panel: HHT
    freq_edges, freq_centres = emd.spectra.define_hist_bins(1, 65, 100, 'log')
    ixh = list(range(14*sample_rate, 16*sample_rate))
    hht = emd.spectra.hilberthuang(IF, IA, freq_edges, mode='amplitude')[:, ixh]
    hht = ndimage.gaussian_filter(hht, 1)#0.5)
    time_v = np.arange(hht.shape[1] - 0.5) / 1250
    
    ax = axhht[i]
    pcm = emd.plotting.plot_hilberthuang(hht, time_v, freq_centres,
                               cmap=newcmp, time_lims=(0, 2),  log_y=True, 
                               vmin=0, vmax=0.3, fig=fig, ax=axhht[i],
                               return_pcm=True)
    ax.minorticks_off()
    ax.set_xticks([0, 0.5, 1, 1.5, 2])
    ax.set_xticklabels(['0', '', '1', '', '2'])
    ax.set_yticks([1, 2, 4, 8, 16, 32, 64])
    ax.set_xlabel('Time [s]', fontsize=8, labelpad=3)
    ax.set_ylabel('Frequency [Hz]')
    
    if i > 0:
        ax2 = fig.add_axes([inset_pos[i], 0.125, 0.2, 0.14])
        # Manually set the position and relative size of the inset axes within ax1
        pcm = emd.plotting.plot_hilberthuang(hht, time_v, freq_centres,
                                   cmap=newcmp, time_lims=(0, 2),  log_y=True, 
                                   vmin=0, vmax=0.3, fig=fig, ax=ax2,
                                   return_pcm=True)
        ax2.minorticks_off()
        ax2.set_xlim(0.25, 0.85)
        ax2.set_ylim(3, 18)
        ax2.set_yticks([4, 8, 16])
        ax2.tick_params(axis='x', labelbottom=False)
        ax2.get_xaxis().set_ticks([])
        #ax2.get_yaxis().set_ticks([])
        ax2.set_ylabel('Frequency [Hz]', labelpad=2)
        ax2.set_xlabel('')
        for child in ax2.get_children():
            if isinstance(child, spines.Spine):
                child.set_color('darkorange')
        #ax2.axis('off')
        # Mark the region corresponding to the inset axes on ax1 and draw lines
        # in grey linking the two axes.
        mark_inset(ax, ax2, loc1=2, loc2=1, edgecolor='darkorange')
    
cax = fig.add_axes([0.92, 0.33, 0.02, 0.135])
cbar = fig.colorbar(pcm, cax=cax)
#cbar.set_ticks([0, 0.1, 0.2, 0.3, 0.4, 0.5])
cbar.ax.tick_params(labelsize=7)
cbar.set_label('Amplitude', fontsize=7)

# 7G: STFT / Wavelet
y = np.sum(imf_all[0, 14*sample_rate:16*sample_rate, :], axis=1)
w = 4.
ftf = freq_centres.copy()#np.linspace(1, 64, 100)
widths = w*sample_rate / (2*ftf*np.pi)
cwtm = signal.cwt(y, signal.morlet2, widths, w=w)
ftt = np.linspace(14, 16, cwtm.shape[1])
ax10.pcolormesh(ftt, ftf, np.abs(cwtm), cmap=newcmp, shading='nearest')
ax10.set_yscale('log')
ax10.minorticks_off()
ax10.set_ylim(1, 64)
ax10.set_xticks([14, 14.5, 15, 15.5, 16])
ax10.set_xticklabels(['0', '', '1', '', '2'])
ax10.set_yticks([1, 2, 4, 8, 16, 32, 64])
ax10.set_yticklabels(['1', '2', '4', '8', '16', '32', '64'])
ax10.set_xlabel('Time [s]', fontsize=8, labelpad=3)
ax10.set_ylabel('Frequency [Hz]')

if save:
    fname = 'Fig7.tiff'
    fig.savefig(fig_dir + fname, format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
    
plt.show()
