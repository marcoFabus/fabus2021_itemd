#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to analyse simulated behaviour of itEMD.
Produces data for Figures 1-6 in Fabus et al (2021).

@author: MSFabus
"""
import numpy as np
import emd
from Tools import it_emd, utils, analysis, sim

out_dir = './Data/'

#%% ## Figure 1: Limitations of EMD

## 1B,C,D: EMD with noise and a burst
sim3 = sim.sim(t=2, sample_rate=512)
f_m = 2
f_gt_m = 4.214
am = np.sin(2*np.pi*f_m*(sim3.time_vect-0.2))
am[am < 0] = 0
am[int(sim3.sample_rate*0.6) : ] = 0

x = sim3.sim_sig(N_comp=2, amps=[1, 1], f=[4, 30], shape=['it_sin', 'sin'], 
                  noise=False, amp_mod=[1, am], it_sin_order=8)

#Ground truth
IP0,IFgt,IA0 = emd.spectra.frequency_stats(sim3.x.T, sim3.sample_rate, 'nht' )
good_cycles = emd.cycles.get_cycle_inds(IP0, return_good=True)
IF0_pa = emd.cycles.phase_align(IP0[:, 0], IFgt[:, 0], cycles=good_cycles[:, 0])
IFgt_pa_m = np.mean(IF0_pa, axis=1)

N = 100
IFc_all = np.zeros((N, len(IFgt_pa_m)))
IFs_all = np.zeros((N, len(IFgt_pa_m)))
IFe_all = np.zeros((N, len(IFgt_pa_m)))
mask_all = np.zeros((N, 6, 10))
IF_all = np.zeros((N, 3, 48))

for i in range(N):
    print(i)
    x = sim3.sim_sig(N_comp=2, amps=[1, 1], f=[4, 30], shape=['it_sin', 'sin'], 
                  noise=True, amp_mod=[1, am],
                  noise_type='white', a_noise=1/16, it_sin_order=8, seed=i)
    
    for k in range(2): 
        if k == 0:
            imf = emd.sift.sift(x, max_imfs=6)
                
        if k == 1:
            mask = np.array([128, 30, 4, 0.01])
            imf = emd.sift.mask_sift(x, max_imfs=4, mask_freqs=mask/sim3.sample_rate)
            
        IP,IF,IA = emd.spectra.frequency_stats(imf, sim3.sample_rate, 'nht')
        m = np.nanargmin(np.abs(np.mean(IF,0)-f_gt_m))
        good_cycles = emd.cycles.get_cycle_inds(IP, return_good=True)
        IF_pa = emd.cycles.phase_align(IP[:, m], IF[:, m], cycles=good_cycles[:, m])
        IF_pa_m = np.mean(IF_pa, axis=1)
        IF_all[i, k, :] = IF_pa_m
    

#Save results
phases = np.linspace(0, 2*np.pi, 48)
np.savez(out_dir +'itEMD_Fig1_data.npz', phases=phases, IFgt=IFgt_pa_m, IFemd=IF_all)
print('\nInitial simulations completed.')


#%% ## Figure 2: Iterated masking intro

##Simulate 2s of iterated sine signal
sim3 = sim.sim(t=2, sample_rate=512)
f0 = 4
f_m = 2
am = np.sin(2*np.pi*f_m*(sim3.time_vect-0.2))
am[am < 0] = 0
am[int(sim3.sample_rate*0.6) : ] = 0

N = 100
mask_all = np.zeros((N, 6, 15))
max_all = np.zeros((N, 6))

for i in range(N):
    print(i)
    x = sim3.sim_sig(N_comp=2, amps=[1, 1], f=[f0, 30], shape=['it_sin', 'sin'], 
                  noise=True, amp_mod=[1, am],
                  noise_type='white', a_noise=1/16, seed=0, it_sin_order=8)
    
    np.random.seed(i)
    m0 = np.random.randint(1, 128, size=6) / sim3.sample_rate
    np.random.seed(0)
    
    nit, m_all, imf, mask_eq, mask_std = it_emd.it_emd(sim3.sig, N_imf=6, 
                                                      N_iter_max=15, N_avg=1, 
                                                      sample_rate=sim3.sample_rate,
                                                      verbose=True, mask_0=m0,
                                                      iter_th=0.01)
    N_imf_f = imf.shape[1]
    mask_all[i, :, :] = m_all.T
    max_all[i, :N_imf_f] = np.max(imf, axis=0)

max_all_avg = np.nanmean(max_all, axis=0)
itr = np.arange(15)

np.savez(out_dir + 'itEMD_Fig2_data.npz', itr=itr, mask=mask_all, w=max_all_avg)
print('\nRandom mask simulations completed.')


#%% ## Figure 3: Noise Simulations
# Shape correlation with ground truth and PMSI vs white noise amplitude
N1 = 100
N2 = 100
corr_all = np.zeros((N1, 4, N2))
noise_all = np.linspace(1.5, 3, N2) #0.01
pmsi_all = np.zeros((N1, 4, N2))
flag_all = np.zeros((N1, N2))
niters_all = np.zeros((N1, N2))

sim3 = sim.sim(t=10, sample_rate=512)
f0 = 4
f_gt_m = 4.214

x = sim3.sim_sig(N_comp=1, amps=[1], f=[f0], shape=['it_sin'], 
                 noise=False, amp_mod=[1], it_sin_order=8) 

#Ground truth
IP0,IFgt,IA0 = emd.spectra.frequency_stats(sim3.x.T, sim3.sample_rate, 'nht' )
good_cycles = emd.cycles.get_cycle_vector(IP0, return_good=True)
IF0_pa = emd.cycles.phase_align(IP0[:, 0], IFgt[:, 0], cycles=good_cycles[:, 0])[0]
IFgt_pa_m = np.mean(IF0_pa, axis=1)

for i in range(N1):
    print('\n', i)
    for j in range(N2):
        print(j, end=' ')
        x = sim3.sim_sig(N_comp=1, amps=[1], f=[f0], shape=['it_sin'], 
                     noise=True, amp_mod=[1], it_sin_order=8,
                     noise_type='white', a_noise=noise_all[j], seed=i)

        for k in range(3): 
            if k == 0:
                imf = emd.sift.mask_sift(x, max_imfs=6, mask_freqs='zc')
            if k == 1:
                imf = emd.sift.ensemble_sift(x, max_imfs=6)
            if k == 2:
                imf, mask_eq, mask_std, niters, flag = it_emd.it_emd(x, N_imf=6, 
                                          N_iter_max=15, N_avg=1, 
                                          iter_th=0.1, mask_0='zc') 
                flag_all[i, j] = flag
                niters_all[i, j] = niters
                
            IP,IF,IA = emd.spectra.frequency_transform(imf, sim3.sample_rate, 'nht')
            m = np.nanargmin(np.abs(np.mean(IF,0)-f_gt_m))

            good_cycles = emd.cycles.get_cycle_vector(IP[:, m], return_good=True)
            IF_pa = emd.cycles.phase_align(IP[:, m], IF[:, m], cycles=good_cycles)[0]
            IF_pa_m = np.mean(IF_pa, axis=1)
                        
            corr_all[i, k, j] = np.corrcoef(IFgt_pa_m, IF_pa_m)[0,1]
            pmsi_all[i, k, j] = analysis.PMSI(imf, m)  

#Save results 
np.savez(out_dir + 'itEMD_Fig3_data.npz', noise=noise_all, corr=corr_all, 
         pmsi=pmsi_all, niters=niters_all,
         flag=flag_all)
print('\nNoise simulations completed.')


#%% ## Figure 4: Frequency Distortion Simulations
# Shape correlation with ground truth vs frequency distortion
N1 = 100
N2 = 18
corr_all = np.zeros((N1, 4, N2))
pmsi_all = np.zeros((N1, 4, N2))

it_sin_order_all = np.arange(1,N2+1)
f_dist_all = np.zeros(N2)

sim3 = sim.sim(t=10, sample_rate=512)
f0 = 4
f_gt_m = 4.214

for i in range(N1):
    print('\n',i)
    for j in range(N2):
        print(j, end=' ')
        
        x = sim3.sim_sig(N_comp=1, amps=[1], f=[f0], shape=['it_sin'], 
                         noise=True, amp_mod=[1], it_sin_order=it_sin_order_all[j],
                         noise_type='white', a_noise=0.5, seed=i)
        
        #Ground truth
        IP0,IFgt,IA0 = emd.spectra.frequency_stats(sim3.x.T, sim3.sample_rate, 'nht' )
        good_cycles = emd.cycles.get_cycle_inds(IP0, return_good=True)
        IF0_pa = emd.cycles.phase_align(IP0[:, 0], IFgt[:, 0], cycles=good_cycles[:, 0])
        IFgt_pa_m = np.mean(IF0_pa, axis=1)

        f_dist = (np.amax(IFgt_pa_m) - np.amin(IFgt_pa_m)) / f0 * 100
        f_dist_all[j] = f_dist
        
        for k in range(3):
            
            if k == 0:
                imf = emd.sift.mask_sift(x, max_imfs=6, mask_freqs='zc')
            if k == 1:
                imf = emd.sift.ensemble_sift(x, max_imfs=6)
            if k == 2:
                imf, mask_eq, mask_std =  it_emd.it_emd(x, N_imf=6, 
                                          N_iter_max=15, N_avg=1, 
                                          iter_th=0.1, mask_0='zc')
                
            IP,IF,IA = emd.spectra.frequency_transform(imf, sim3.sample_rate, 'nht')
            m = np.nanargmin(np.abs(np.mean(IF,0)-f_gt_m))
            good_cycles = emd.cycles.get_cycle_inds(IP, return_good=True)            
            IF_pa = emd.cycles.phase_align(IP[:, m], IF[:, m], cycles=good_cycles[:, m])
            IF_pa_m = np.mean(IF_pa, axis=1)
            corr_all[i, k, j] = np.corrcoef(IFgt_pa_m, IF_pa_m)[0,1]
            pmsi_all[i, k, j] = analysis.PMSI(imf, m) 

#Save results 
np.savez(out_dir + 'itEMD_Fig4_data.npz', freq_dist=f_dist_all, corr=corr_all, pmsi=pmsi_all)
print('\nFrequency distortion simulations completed.')


#%% Figure 5D: Detailed waveshape simulation

# Prepare arrays
IF_all = np.zeros((3, 48))
IF_std_all = np.zeros((3, 48))
N_all = np.zeros(3)
rmse_all = np.zeros((3, 80))*np.nan
pmsi_all = np.zeros(3)
f_gt_m = 4.214
secs = [17,19]

sim3 = sim.sim(t=20, sample_rate=512)
x = sim3.sim_sig(N_comp=1, amps=[1], f=[4], shape=['it_sin'], 
             noise=True, amp_mod=[1], it_sin_order=4,
             noise_type='white', a_noise=0.1, seed=0)

for k in range(3): # Dyadic, Ensemble, itEMD
    if k == 0:
        imf = emd.sift.mask_sift(x, max_imfs=6, mask_freqs='zc', mask_amp_mode='ratio_sig')
    if k == 1:
        imf = emd.sift.ensemble_sift(x, max_imfs=6)
    if k == 2:
        imf, mask_eq, mask_std, niter, flag = it_emd.it_emd(x, N_imf=6, 
                                  N_iter_max=15, N_avg=1, 
                                  iter_th=0.1, mask_0='zc', ignore_last=True) 
        phases = np.linspace(0, 2*np.pi, 48)

        #Ground truth
        IP0,IFgt,IA0 = emd.spectra.frequency_transform(sim3.x.T, sim3.sample_rate, 'nht' )
        good_cycles = emd.cycles.get_cycle_inds(IP0, return_good=True)
        IF0_pa = emd.cycles.phase_align(IP0[:, 0], IFgt[:, 0], cycles=good_cycles[:, 0])[0]
        IFgt_pa_m = np.mean(IF0_pa, axis=1)
        
        #EMD
        IP,IF,IA = emd.spectra.frequency_transform(imf, sim3.sample_rate, 'nht')
        m = np.nanargmin(np.abs(np.mean(IF,0)-f_gt_m))
        msk = np.logical_and(IF[:, m] < 6, IF[:, m] > 2.5)
        good_cycles = emd.cycles.get_cycle_inds(IP, return_good=True, mask=msk)
        IF_pa = emd.cycles.phase_align(IP[:, m], IF[:, m], cycles=good_cycles[:, m])[0]
        IF_pa_m = np.mean(IF_pa, axis=1)
        IF_pa_std = np.std(IF_pa, axis=1)
        
        IF_all[k, :] = IF_pa_m
        IF_std_all[k, :] = IF_pa_std
        N = int(good_cycles[:,m].max())+1
        N_all[k] = N
        
        rmse_all[k, :N] = np.array([utils.rmse(x, IFgt_pa_m) for x in IF_pa.T]) 
        pmsi_all[k] = analysis.PMSI(imf, m) 

np.savez(out_dir + 'itEMD_Fig5_data.npz', phases=phases, IFgt=IFgt_pa_m, 
         IFemd=IF_all, IFstd=IF_std_all, N=N_all, rmse=rmse_all, pmsi=pmsi_all)
print('\nFigure 5 waveshape simulations completed.')


#%% ## Figure 6: Sparsity Simulations
# Fixed noise and shape distortion, changing number of cycles averaged
N1 = 100
N2 = 100
corr_all = np.zeros((N1, 4, N2))
pmsi_all = np.zeros((N1, 4, N2))

T = 25
f0 = 4
f_gt_m = 4.214
sim3 = sim.sim(t=T, sample_rate=512)
x = sim3.sim_sig(N_comp=1, amps=[1], f=[f0], shape=['it_sin'], 
                 noise=False, amp_mod=[1], it_sin_order=8) #white

#Ground truth
IP0,IFgt,IA0 = emd.spectra.frequency_stats(sim3.x.T, sim3.sample_rate, 'nht' )
good_cycles = emd.cycles.get_cycle_inds(IP0, return_good=True)
IF0_pa = emd.cycles.phase_align(IP0[:, 0], IFgt[:, 0], cycles=good_cycles[:, 0])
IFgt_pa_m = np.mean(IF0_pa, axis=1)

frac_all = np.linspace(0.1, 0.95, N2)
num_osc = frac_all * T * f0
a = np.zeros(sim3.N_s)

for i in range(N1):
    print('\n',i)
    for j in range(N2):
        print(j, end=' ')
        a = np.zeros(sim3.N_s)
        idx = list(range(int(0.25*sim3.sample_rate), int(sim3.N_s*frac_all[j] + 0.25*sim3.sample_rate))) #0.25s to frac+0.25s
        a[idx] = 1 
        x = sim3.sim_sig(N_comp=1, amps=[1], f=[f0], shape=['it_sin'], 
                         noise=True, amp_mod=[a], it_sin_order=8,
                         noise_type='white', a_noise=1, seed=i)
        
        for k in range(3): 
            if k == 0:
                imf = emd.sift.mask_sift(x, max_imfs=6, mask_freqs='zc')
            if k == 1:
                imf = emd.sift.ensemble_sift(x, max_imfs=6)
            if k == 2:
                imf, mask_eq, mask_std =  it_emd.it_emd(x, N_imf=6, 
                                          N_iter_max=15, N_avg=1, 
                                          iter_th=0.1, mask_0='zc')
                
            IP,IF,IA = emd.spectra.frequency_stats(imf, sim3.sample_rate, 'nht')
            m = np.nanargmin(np.abs(np.mean(IF[idx,:],0)-f_gt_m))
            msk = IA[:,m] > 3 * np.std(imf[a==0,m])
            good_cycles = emd.cycles.get_cycle_inds(IP, return_good=True, mask=msk)
            IF_pa = emd.cycles.phase_align(IP[:, m], IF[:, m], cycles=good_cycles[:, m])
            IF_pa_m = np.mean(IF_pa, axis=1)
            corr_all[i, k, j] = np.corrcoef(IFgt_pa_m, IF_pa_m)[0,1]
            pmsi_all[i, k, j] = analysis.PMSI(imf, m) 
            
np.savez(out_dir + 'itEMD_Fig6_data.npz', num_osc=num_osc, corr=corr_all, pmsi=pmsi_all)
print('\nSparsity simulations completed.')