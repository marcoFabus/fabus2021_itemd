#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to analyse hippocampal LFP from CRCNS hc-3 rat data.
Produces data for Figure 7.

@author: MSFabus
"""
import numpy as np
from scipy import signal
from Tools import it_emd, utils, analysis
import emd

## Directory set-up
out_dir = './Data/'
fig_dir = './Figures/'
data_dir = '/media/marco/MSFabus_DPhil1/Datasets/itEMD/hc-3/ec013.30/ec013.454/ec013.454.eeg'

# Basic info and data loading
chans = 65
raw = np.fromfile(data_dir, dtype=np.int16).astype(float)
raw = raw.reshape(-1, chans)[:, 10]
data = raw[:1000*1250] / 1000
sample_rate = 1250
N_imf = 6
N_seg = 20

# Find spectral peak
f, p = utils.plot_psd(data, sample_rate, return_psd=True, nperseg=8*sample_rate)
f_theta = np.abs(f-6) < 2                      # 4-8Hz theta
f_peak = f[f_theta][signal.find_peaks(p[f_theta], distance=48)[0]][0]

# Prepare data segmentation
ts = np.linspace(0, len(data)/sample_rate, len(data))
segments = np.linspace(0, 1000, N_seg+1)

# Prepare output
wfs = np.zeros((6, 48))
pmsi = np.zeros((3, N_seg))
N_w = np.zeros(3)
out = [[] for _ in range(len(segments)-1)]
imf_all = np.zeros((3, len(data), N_imf))*np.nan
mask_all = np.zeros((len(segments)-1, N_imf))*np.nan
niters_all = np.zeros(len(segments)-1)*np.nan

print('\n Processing segment:')

ctr = 0
for s in range(len(segments)-1):
    print('\n %s / %s' %(s+1, len(segments)-1))
    
    # Select slice
    ROI = np.logical_and(ts > segments[s], ts < segments[s+1])
    x = data[ROI]
    time_vect = ts[ROI]
         
    for i in range(3):
        if i == 0:
        # itEMD
            [imf, mask_eq, mask_var, niters, _] = it_emd.it_emd(x, sample_rate=sample_rate,
                                                                   N_imf=6, N_avg=1,
                                                                   iter_th=0.1)
            out[s] = [imf, mask_eq, mask_var, time_vect, niters]
            N = imf.shape[1]
            mask_all[s, :N] = mask_eq
            niters_all[s] = niters   
        if i == 1:
            imf = emd.sift.mask_sift(x, mask_freqs='zc', max_imfs=N_imf)
        if i == 2:
            imf = emd.sift.ensemble_sift(x, max_imfs=N_imf)
        
        # Pick IMF closest to spectral peak
        IP, IF, IA = emd.spectra.frequency_transform(imf, sample_rate, 'nht')
        avg_f = np.round(np.mean(IF, axis=0), 2)
        ind = np.argmin(np.abs(avg_f - f_peak))
        
        # Calculate PMSI and save imfs
        N = imf.shape[1]
        pmsi[i, s] = analysis.PMSI(imf, ind)
        imf_all[i, ctr:ctr+len(x), :N] = imf
        
    ctr += len(x)
    
# Find cycles and their shape   
for i in range(3):

    imf = imf_all[i, :, :N_imf]
    keep = ~np.isnan(imf).all(axis=1)
    imf = imf[keep, :]
    
    # Spectrum
    IP, IF, IA = emd.spectra.frequency_transform(imf, sample_rate, 'nht')
    
    avg_f = np.round(np.mean(IF, axis=0), 2)
    ind = np.argmin(np.abs(avg_f - f_peak))
    
    freq_edges, freq_bins = emd.spectra.define_hist_bins(0,128,256)
    spec = emd.spectra.hilberthuang_1d(IF, IA, freq_edges)
    
    # Find big cycles
    amp_th = IA[:,ind] > np.percentile(IA[:,ind],50)
    f_th = IF[:, ind] < 16
    msk = np.logical_and(f_th, amp_th)
    cycles = emd.cycles.get_cycle_vector(IP[:,ind], return_good=True, mask=f_th)
    N_w[i] = cycles.max()
    
    # Run phase align
    pa, ph = emd.cycles.phase_align(IP[:,ind], IF[:,ind], cycles=cycles)

    wfs[i, :] = pa.mean(axis=1)
    wfs[i+3, :] = np.std(pa, axis=1)

fname = out_dir + 'itEMD_Fig7_data.npz'
np.savez(fname, pmsi=pmsi, imf=imf_all, wfs=wfs, N_w=N_w, niters=niters_all,
          ph=ph)

print('LFP analysis complete.')