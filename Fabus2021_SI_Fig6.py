#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script plots MEG data results (Figures 8) from saved files.

@author: MSFabus
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats, io
from statsmodels.sandbox.stats.multicomp import multipletests
import seaborn as sns 
import statsmodels.api as sm
from Tools import it_emd
import emd

## Plotting options
save = False
plt.rcParams['figure.dpi'] = 200
dpi = 200
data_dir = './Data_prebaked/SI/'
fig_dir = './Figures/SI/'

#Labels
labs = ['itEMD', 'dyadic sift', 'ensemble sift']
cols = ['green', 'darkorange', 'cornflowerblue']

# Load data
data = np.load(data_dir + 'itEMD_MEG_data_1seg.npz')
ph = data['phases']
wfs = data['wfs']
pmsi = data['pmsi']
fpeak = data['f_peak']
niters = data['niters']


#%% 8A: PMSI violin plot

pmsi = np.delete(pmsi, [2], axis=0)
pvalue = np.zeros(3)*np.nan
_ , pvalue[0] = stats.ttest_rel(pmsi[:, 0], pmsi[:, 1], nan_policy='omit')
_ , pvalue[1] = stats.ttest_rel(pmsi[:, 0], pmsi[:, 2], nan_policy='omit')
_ , pvalue[2] = stats.ttest_rel(pmsi[:, 1], pmsi[:, 2], nan_policy='omit')

pvalue *= 2 #one-tailed result
reject, corrpvalue, alphacSidak, alphacBonf = multipletests(pvalue, alpha=0.05, method='bonferroni')

plt.rc('font', size=24)
fig, ax = plt.subplots(figsize=(6, 8))
violin_parts = ax.violinplot(pmsi, showextrema=False, showmedians=True)
ct = 0
for pc in violin_parts['bodies']:
    pc.set_facecolor(cols[ct])
    pc.set_edgecolor('black')
    pc.set_alpha(1)
    ct += 1

violin_parts['cmedians'].set_edgecolor('black')
violin_parts['cmedians'].set_linewidth(2)

ax.plot([1, 2], [0.27, 0.27], color='k', lw=4)
ax.plot([1, 3], [0.3, 0.3], color='k', lw=4)
ax.plot([2, 3], [0.24, 0.24], color='k', lw=4)

ax.set_ylabel('PMSI')
plt.rc('font', size=24)
ax.set_xticks([1, 2, 3])
ax.set_yticks([0, 0.1, 0.2, 0.3])
ax.set_xticklabels(['itEMD', 'Dyadic', 'EEMD'])
ax.tick_params(axis='x', pad=15)
plt.tight_layout()

if save:
    fname = 'FigS6A.tiff'
    fig.savefig(fname, format='tiff', bbox_inches='tight',
        pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)


#%% 8B: Average phase-aligned IF
wfm = np.nanmean(wfs, axis=0)
wfe = np.nanstd(wfs, axis=0) / np.sqrt(9)

plt.rc('font', size=24)
fig, ax = plt.subplots(figsize=(6, 8))
for k in range(3):
    y1 = wfm[k, :]
    er1 = wfe[k, :]
    ax.plot(ph, y1, label=labs[k], linewidth=4, color=cols[k])
    ax.fill_between(ph, y1-er1, y1+er1, alpha=0.5, color=cols[k])

ax.set_xlabel('Phase [rad]')
ax.set_ylabel('IF [Hz]')
plt.rc('font', size=16)
leg = ax.legend(ncol=1)
for line in leg.get_lines():
    line.set_linewidth(8.0)
axx = plt.gca().add_artist(leg)
ax.set_xticks([0, np.pi, 2*np.pi])
plt.rc('text', usetex=True)
ax.set_xticklabels([0, '$\pi$', '$2 \pi$'])
plt.rc('text', usetex=False)
ax.set_xlim(0, 2*np.pi)
ax.set_ylim(8.5, 11.5)
ax.set_yticks([9, 10, 11])
ax.tick_params(axis='x', pad=15)
ax.grid(True)
plt.tight_layout()

if save:
    fname = 'FigS6B.tiff'
    fig.savefig(fig_dir + fname, format='tiff', bbox_inches='tight',
        pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)

plt.show()


#%% 8D: Peak frequencies in Fourier vs itEMD
plt.rc('font', size=24)
fig, ax = plt.subplots(figsize=(6, 8))
occ_f = fpeak

fpeak_emd = np.mean(wfs, axis=2)
fstd_emd = np.std(wfs, axis=2)

occ_f_emd = fpeak_emd[:, 0]
occ_std_emd = fstd_emd[:, 0]

X2 = sm.add_constant(occ_f)
est = sm.OLS(occ_f_emd, X2, missing='drop')
res = est.fit(); print(res.summary())
ypred = res.predict(X2)

sns.regplot(x=occ_f, y=occ_f_emd, label=labs[0], color=cols[0])
ax.set_xlabel('Fourier peak f [Hz]')
ax.set_ylabel('itEMD shape f [Hz]')
plt.rc('font', size=24)
ax.set_xticks([8, 9, 10, 11])
ax.set_yticks([8, 9, 10, 11])
ax.set_ylim(8.5, 11.5)
ax.set_xlim(8.5, 11.5)
ax.tick_params(axis='x', pad=15)
ax.grid(True) 
plt.rc('font', size=14) 

plt.tight_layout()

if save:
    fname = 'FigS6D.tiff'
    fig.savefig(fig_dir + fname, format='tiff', bbox_inches='tight',
        pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)

plt.show()


#%% 8C, 8E: Example spectrum and decomposition
# Load data
fpath = data_dir + 'sub-10_data.mat'
F = io.loadmat(fpath)
data = np.array( F['occ'].flatten())
sample_rate = 400

# Compute itEMD
t = np.linspace(0, len(data)/sample_rate, len(data))
segs = np.linspace(0, t[-1], 2) 

[imf, mask_eq, mask_std, n_it] = it_emd.it_emd_seg(data, t, segs, 
                          N_imf=8, joint=True,
                          N_iter_max=15, N_avg=1, 
                          iter_th=0.1, mask_0='zc', 
                          sample_rate=sample_rate,
                          w_method='power', ignore_last=True)

# 8E: Example decomposition
fig = it_emd.plot_imf(imf[:, :5], secs=[20, 25], sample_rate=sample_rate)
if save:
    fname = 'FigS6E.tiff'
    fig.savefig(fig_dir + fname, format='tiff', bbox_inches='tight',
        pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
plt.show()