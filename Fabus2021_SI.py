#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to analyse additional behaviour of itEMD.
Produces data for Supplementary Figures in Fabus et al (2021).

@author: MSFabus
"""
import numpy as np
import emd
from Tools import it_emd, utils, analysis, sim

out_dir = './Data/SI/'

#%% ## Figure S2-A: itEMD less bandwidth limited
N1 = 100
N2 = 100
noise_all = np.linspace(0.01, 3, N2)
sbw_all = np.zeros((N1, 3, N2))

sim3 = sim.sim(t=10, sample_rate=512)
f0 = 4
f_avg = 4.214

for i in range(N1):
    print('\n',i)
    for j in range(N2):
        print(j, end=' ')
        
        x = sim3.sim_sig(N_comp=1, amps=[1], f=[f0], shape=['it_sin'], 
                          noise=True, amp_mod=[1], it_sin_order=8,
                          noise_type='white', a_noise=noise_all[j], seed=i) 
        p_all = np.zeros((3, 1025))
        
        for k in range(3): 
            if k == 0:
                imf = emd.sift.mask_sift(x, max_imfs=6, mask_freqs='zc')
            if k == 1:
                imf = emd.sift.ensemble_sift(x, max_imfs=6)
            if k == 2:
                imf, mask_eq, mask_std =  it_emd.it_emd(x, N_imf=6, 
                                          N_iter_max=15, N_avg=1, 
                                          iter_th=0.1,
                                          mask_0='zc', sample_rate=512) 
            
            IP,IF,IA = emd.spectra.frequency_stats(imf, sim3.sample_rate, 'nht')
            m = np.nanargmin(np.abs(np.mean(IF,0)-f_avg))
            [f, p_imf] = utils.plot_psd(imf[:,m], sim3.sample_rate, return_psd=True)
            p_all[k, :] = p_imf
            
            # Remove main peaks for spectral edge estimate
            p_all[k, 15:18] = np.mean(p_imf[[13, 14, 18, 19]])
            p_all[k, 47:50] = np.mean(p_imf[[46, 50]])
            
            p = p_all[k, :]
            cum_p = np.cumsum(p) / np.sum(p)
            sef = f[np.argmin(np.abs(cum_p-0.95))] #Spectral edge frequency
            sbf = f[np.argmin(np.abs(cum_p-0.05))] #Spectral beginning frequency
            sbw_all[i, k, j] = sef - sbf

np.savez(out_dir + 'itEMD_FigS2A_data.npz', noise=noise_all, sbw=sbw_all)
print('\nBandwidth simulations completed.')


#%%## Figure S2-C: SNR boost v white noise amplitude

N1 = 100
N2 = 100
res_all = np.zeros((N1, 3, N2, 2))
res_0 = np.zeros((N2, 2))
noise_all = np.linspace(0.01, 3, N2)
fs = 512
sim3 = sim.sim(t=25, sample_rate=fs)
f0 = 4
f_avg = 4.214

a = np.zeros(25*fs)
idx = list(range(int(3*fs), int(5.5*fs))) #10 cycles
idxn = list(range(int(6*fs), int(9*fs))) #10 cycles
a[idx] = 1 

for i in range(N1):
    print('\n',i)
    for j in range(N2):
        print(j, end=' ')
        x = sim3.sim_sig(N_comp=1, amps=[1], f=[f0], shape=['it_sin'], 
                     noise=True, amp_mod=[a],
                     noise_type='white', a_noise=noise_all[j], seed=i) 
        sig = 1
        n = np.std(sim3.x_n)
        res_0[j, 0] = sig / n
        res_0[j, 1] = n

        for k in range(3): #Dyadic, Ensemble, itEMD
            
            if k == 0:
                imf = emd.sift.mask_sift(x, max_imfs=6, mask_freqs='zc')
            if k == 1:
                imf = emd.sift.ensemble_sift(x, max_imfs=6)
            if k == 2:
                imf, mask_eq, mask_std =  it_emd.it_emd(x, N_imf=6, 
                                          N_iter_max=15, N_avg=1, 
                                          iter_th=0.1,
                                          mask_0='zc', sample_rate=fs) 
                
            IP,IF,IA = emd.spectra.frequency_stats(imf, sim3.sample_rate, 'nht')
            m = np.nanargmin(np.abs(np.mean(IF[idx, :],0)-f0))
            
            n = np.std(imf[idxn, m])
            sig = np.mean(IA[idx, m]) - n
            
            res_all[i, k, j, 0] = sig / n
            res_all[i, k, j, 1] = n   
                        
np.savez(out_dir + 'itEMD_FigS2C_data.npz', noise=noise_all, 
         SNR=res_all[:,:,:,0], SNR_0=res_0[:,0])
print('\nSNRsimulations completed.')


#%% ## Figure S3 left: Brown noise simulations
# Shape correlation with ground truth and PMSI vs brown noise amplitude
N1 = 100
N2 = 100
corr_all = np.zeros((N1, 4, N2))
noise_all = np.linspace(0.01, 3, N2)
pmsi_all = np.zeros((N1, 4, N2))

sim3 = sim.sim(t=10, sample_rate=512)
f0 = 4
f_gt_m = 4.214

x = sim3.sim_sig(N_comp=1, amps=[1], f=[f0], shape=['it_sin'], 
                 noise=False, amp_mod=[1], it_sin_order=8) 

#Ground truth
IP0,IFgt,IA0 = emd.spectra.frequency_stats(sim3.x.T, sim3.sample_rate, 'nht' )
good_cycles = emd.cycles.get_cycle_inds(IP0, return_good=True)
IF0_pa = emd.cycles.phase_align(IP0[:, 0], IFgt[:, 0], cycles=good_cycles[:, 0])
IFgt_pa_m = np.mean(IF0_pa, axis=1)

for i in range(N1):
    print('\n',i)
    for j in range(N2):
        print(j, end=' ')
        x = sim3.sim_sig(N_comp=1, amps=[1], f=[f0], shape=['it_sin'], 
                     noise=True, amp_mod=[1], it_sin_order=8,
                     noise_type='brown', a_noise=noise_all[j], seed=i) 

        for k in range(3): 
            if k == 0:
                imf = emd.sift.mask_sift(x, max_imfs=6, mask_freqs='zc')
            if k == 1:
                imf = emd.sift.ensemble_sift(x, max_imfs=6)
            if k == 2:
                imf, mask_eq, mask_std =  it_emd.it_emd(x, N_imf=6, 
                                          N_iter_max=15, N_avg=1, 
                                          iter_th=0.1,
                                          mask_0='zc', sample_rate=512) 
                
            IP,IF,IA = emd.spectra.frequency_stats(imf, sim3.sample_rate, 'nht')
            m = np.nanargmin(np.abs(np.mean(IF,0)-f_gt_m))
            good_cycles = emd.cycles.get_cycle_inds(IP, return_good=True)
            IF_pa = emd.cycles.phase_align(IP[:, m], IF[:, m], cycles=good_cycles[:, m])
            IF_pa_m = np.mean(IF_pa, axis=1)
            corr_all[i, k, j] = np.corrcoef(IFgt_pa_m, IF_pa_m)[0,1]
            pmsi_all[i, k, j] = analysis.PMSI(imf, m)  

#Save results 
np.savez(out_dir + 'itEMD_Fig_S3_noise_data.npz', noise=noise_all, 
         corr=corr_all, pmsi=pmsi_all)
print('\nNoise simulations completed.')


#%% ## Figure S3 middle: Frequency Distortion Simulations
# Shape correlation with ground truth vs frequency distortion
N1 = 100
N2 = 18
corr_all = np.zeros((N1, 4, N2))
pmsi_all = np.zeros((N1, 4, N2))

it_sin_order_all = np.arange(1,N2+1)
f_dist_all = np.zeros(N2)

sim3 = sim.sim(t=10, sample_rate=512)
f0 = 4
f_gt_m = 4.214

for i in range(5,N1):
    print('\n',i)
    for j in range(N2):
        print(j, end=' ')
        
        x = sim3.sim_sig(N_comp=1, amps=[1], f=[f0], shape=['it_sin'], 
                         noise=True, amp_mod=[1], it_sin_order=it_sin_order_all[j],
                         noise_type='brown', a_noise=1, seed=i) 
        
        #Ground truth
        IP0,IFgt,IA0 = emd.spectra.frequency_stats(sim3.x.T, sim3.sample_rate, 'nht' )
        good_cycles = emd.cycles.get_cycle_inds(IP0, return_good=True)
        IF0_pa = emd.cycles.phase_align(IP0[:, 0], IFgt[:, 0], cycles=good_cycles[:, 0])
        IFgt_pa_m = np.mean(IF0_pa, axis=1)

        f_dist = (np.amax(IFgt_pa_m) - np.amin(IFgt_pa_m)) / f0 * 100
        f_dist_all[j] = f_dist
        
        for k in range(3): 
            if k == 0:
                imf = emd.sift.mask_sift(x, max_imfs=6, mask_freqs='zc')
            if k == 1:
                imf = emd.sift.ensemble_sift(x, max_imfs=6)
            if k == 2:
                imf, mask_eq, mask_std =  it_emd.it_emd(x, N_imf=6, 
                                          N_iter_max=15, N_avg=1, 
                                          iter_th=0.1,
                                          mask_0='zc', sample_rate=512)
                
            IP,IF,IA = emd.spectra.frequency_stats(imf, sim3.sample_rate, 'nht')
            m = np.nanargmin(np.abs(np.mean(IF,0)-f_gt_m))
            good_cycles = emd.cycles.get_cycle_inds(IP, return_good=True)

            waveform = np.zeros((512, good_cycles[:, m].max()))*np.nan
            
            for ii in range(1, good_cycles[:, m].max()+1):
                inds = good_cycles[:, m] == ii
                waveform[:np.sum(inds), ii-1] = imf[inds, m]
            
            IF_pa = emd.cycles.phase_align(IP[:, m], IF[:, m], cycles=good_cycles[:, m])
            IF_pa_m = np.mean(IF_pa, axis=1)
            corr_all[i, k, j] = np.corrcoef(IFgt_pa_m, IF_pa_m)[0,1]
            pmsi_all[i, k, j] = analysis.PMSI(imf, m) 

#Save results 
np.savez(out_dir + 'itEMD_FigS3_FD_data.npz', freq_dist=f_dist_all, corr=corr_all, pmsi=pmsi_all)
print('\nFrequency distortion simulations completed.')


#%% ## Figure S3 right: Sparsity simulations
# Fixed noise and shape distortion, changing number of cycles averaged
N1 = 100
N2 = 100
corr_all = np.zeros((N1, 4, N2))
pmsi_all = np.zeros((N1, 4, N2))

T = 25
f0 = 4
f_gt_m = 4.214
sim3 = sim.sim(t=T, sample_rate=512)
x = sim3.sim_sig(N_comp=1, amps=[1], f=[f0], shape=['it_sin'], 
                 noise=False, amp_mod=[1], it_sin_order=8) 

#Ground truth
IP0,IFgt,IA0 = emd.spectra.frequency_stats(sim3.x.T, sim3.sample_rate, 'nht' )
good_cycles = emd.cycles.get_cycle_inds(IP0, return_good=True)
IF0_pa = emd.cycles.phase_align(IP0[:, 0], IFgt[:, 0], cycles=good_cycles[:, 0])
IFgt_pa_m = np.mean(IF0_pa, axis=1)

frac_all = np.linspace(0.1, 0.95, N2)
num_osc = frac_all * T * 4
a = np.zeros(sim3.N_s)

for i in range(N1):
    print('\n',i)
    for j in range(N2):
        print(j, end=' ')
        a = np.zeros(sim3.N_s)
        idx = list(range(int(0.25*sim3.sample_rate), int(sim3.N_s*frac_all[j] + 0.25*sim3.sample_rate))) #0.25s to frac+0.25s
        a[idx] = 1 
        x = sim3.sim_sig(N_comp=1, amps=[1], f=[f0], shape=['it_sin'], 
                         noise=True, amp_mod=[a], it_sin_order=8,
                         noise_type='brown', a_noise=1, seed=i)
        
        for k in range(3): 
            if k == 0:
                imf = emd.sift.mask_sift(x, max_imfs=6, mask_freqs='zc')
            if k == 1:
                imf = emd.sift.ensemble_sift(x, max_imfs=6)
            if k == 2:
                imf, mask_eq, mask_std = it_emd.it_emd(x, N_imf=6, 
                                          N_iter_max=15, N_avg=1, 
                                          iter_th=0.1,
                                          mask_0='zc', sample_rate=512)
                
            IP,IF,IA = emd.spectra.frequency_stats(imf, sim3.sample_rate, 'nht')
            m = np.nanargmin(np.abs(np.mean(IF[idx,:],0)-f_gt_m))
            msk = IA[:,m] > 3 * np.std(imf[a==0,m])
            good_cycles = emd.cycles.get_cycle_inds(IP, return_good=True, mask=msk)
            IF_pa = emd.cycles.phase_align(IP[:, m], IF[:, m], cycles=good_cycles[:, m])
            IF_pa_m = np.mean(IF_pa, axis=1)
            corr_all[i, k, j] = np.corrcoef(IFgt_pa_m, IF_pa_m)[0,1]
            pmsi_all[i, k, j] = analysis.PMSI(imf, m) 
            
np.savez(out_dir + 'itEMD_FigS3_sparsity_data.npz', num_osc=num_osc, 
         corr=corr_all, pmsi=pmsi_all)
print('\nSparsity simulations completed.')