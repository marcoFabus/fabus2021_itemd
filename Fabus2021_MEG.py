#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to analyse occipital alpha from resting-state MEG.
Produces data for Figure 8.

@author: MSFabus
"""
import os
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal, io
from Tools import it_emd, utils, analysis
import emd

## Plotting options
save = False
plt.rcParams['figure.dpi'] = 200
dpi = 200
out_dir = './Data/'
fig_dir = './Figures/'

# Load data
data_dir = "/media/marco/MSFabus_DPhil1/Datasets/itEMD/MEG/sensor_data/"
sub = os.listdir(data_dir)
N_sub = len(sub)
sample_rate = 400

# Set up results arrays
wfs_group = np.zeros((N_sub, 3, 48))*np.nan
wfs_std_group = np.zeros((N_sub, 3, 48))*np.nan
pmsi_group = np.zeros((N_sub, 3))*np.nan
N_group = np.zeros((N_sub, 3))*np.nan
f_peak_group = np.zeros(N_sub)*np.nan
len_all = np.zeros((N_sub, 2))*np.nan
niter_group = np.zeros((N_sub, 10))*np.nan

for ii in range(N_sub):
    print('Subject: ', ii)
    
    # Subject 3 has no alpha peak, skip
    if ii == 2:
        continue
    
    fpath = data_dir + sub[ii]
    F = io.loadmat(fpath)
    data = np.array( F['occ'].flatten())
    
    # Find spectral peak
    x = data
    f, p = utils.plot_psd(x, sample_rate, return_psd=True, nperseg=8*sample_rate)
    f_alpha = np.abs(f-10) < 2                      # 8-12Hz
    f_peak = f[f_alpha][signal.find_peaks(p[f_alpha], distance=24, prominence=0.1)[0]]    
    f_peak_group[ii] = f_peak if f_peak.shape[0] != 0 else np.nan
    
    t = np.linspace(0, len(x)/sample_rate, len(x))
    segs = np.linspace(0, t[-1], 11)                # Segmentation for data
    len_all[ii, :] = [len(x)/sample_rate, segs[1]]  # Duration and segment length
    N = np.zeros(3)
    
    # Perform EMD
    for k in range(3):
        if k == 0:
            [imf, mask_eq, mask_std, n_it] = it_emd.it_emd_seg(x, t, segs, 
                                      N_imf=8, joint=True,
                                      N_iter_max=15, N_avg=1, iter_th=0.1,
                                      mask_0='zc', sample_rate=sample_rate,
                                      w_method='power', ignore_last=True)
            m = np.nanargmin(np.abs(mask_eq-f_peak))
            niter_group[ii, :] = n_it
        if k == 1:
            mask = 'zc'
            imf = emd.sift.mask_sift(x, max_imfs=6, mask_freqs=mask)
        if k == 2:
            imf = emd.sift.ensemble_sift(x, max_imfs=6)
        
        # Spectrum
        IP, IF, IA = emd.spectra.frequency_transform(imf, sample_rate, 'nht')
        
        if k > 0:
            avg_f = np.mean(IF, axis=0)
            m = np.argmin(np.abs(avg_f-f_peak))
            
        amp_th = IA[:, m] > np.percentile(IA[:, m], 50)
        f_th = np.logical_and(IF[:, m] < 14, IF[:, m] > 7)
        mask = np.logical_and(amp_th, f_th)
        
        # Find avg waveform shape
        cycles = emd.cycles.get_cycle_inds(IP[:,m], return_good=True, mask=mask)
        cycle_freq = emd.cycles.get_cycle_stat(cycles, IF[:, m], mode='compressed', func=np.mean)
        pa = emd.cycles.phase_align(IP[:,m], IF[:,m], cycles=cycles)
        wf = pa.mean(axis=1)
        wf_std = pa.std(axis=1)
        N = cycles.max()
        
        # Save subject-level results
        wfs_group[ii, k, :] = wf
        wfs_std_group[ii, k, :] = wf_std
        N_group[ii, k] = N
                
        #Calculate PMSI
        pmsi_group[ii, k] = analysis.PMSI(imf, m)     
    
    ph = np.linspace(0, 2*np.pi, 48)


fname = 'itEMD_MEG_data_10seg.npz'
np.savez(out_dir + fname, phases=ph, wfs=wfs_group, 
            pmsi=pmsi_group, wfs_std = wfs_std_group,
            N=N_group, f_peak=f_peak_group, niters=niter_group, lens=len_all)

print('\nMEG analysis completed.')


#%% Figure S6-C: Example power spectra
plt.rc('font', size=32)
fig, axs = plt.subplots(3, 1, figsize=(12, 24))
ctr = 0
f_peak_group = np.zeros(N_sub)*np.nan
    
for ii in [1, 3, 5]:
    print('Subject: ', ii)
    if ii == 2:
        continue
    
    fpath = data_dir + sub[ii]
    F = io.loadmat(fpath)
    data = np.array( F['occ'].flatten())
    
    #Plot power spectrum and mark spectral alpha peak
    x = data
    f, p = utils.plot_psd(x, sample_rate, return_psd=True, nperseg=8*sample_rate)
    f_alpha = np.abs(f-10) < 2 #8-12Hz
    f_peak = f[f_alpha][signal.find_peaks(p[f_alpha], distance=24, prominence=0.1)[0]]    
    f_peak_group[ii] = f_peak if f_peak.shape[0] != 0 else np.nan
    
    ax = axs.flatten()[ctr]
    ax.loglog(f, p, lw=2, c='k')
    if not np.isnan(f_peak_group[ii]):
        ax.axvline(f_peak, color='red')
    ax.set_xlim(1, 100)
    ax.set_ylim(0.001, 10)
    ax.set_xticks([1, 10, 100])
    ax.set_xticklabels([1, 10, 100])
    tt = 'Subject ' + str(ii+1)
    ax.set_title(tt)
    ctr += 1
plt.rc('font', size=48)
fig.text(0.5, 0.0, 'Frequency [Hz]', ha='center')
fig.text(0.0, 0.5, 'Power Spectrum [a.u.]' , va='center', rotation='vertical')
plt.tight_layout()

if save:
    fig.savefig(fig_dir + 'FigS6C.tiff', format='tiff', bbox_inches='tight',
                pil_kwargs={"compression": "tiff_lzw"}, dpi=dpi)
    
plt.show()