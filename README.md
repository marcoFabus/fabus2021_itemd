# Fabus2021_itEMD

Analysis scripts accompanying Fabus et al (2021).

# Requirements

The LFP data used in this analysis can be freely downloaded from the CRCNS website (https://crcns.org/data-sets/hc/hc-3/, requires free registration).

The MEG data used in this analysis can be freely downloaded from https://camcan-archive.mrc-cbu.cam.ac.uk/dataaccess/.

The original analysis used Python 3.9.4. The package requirements to exactly reproduce the the original analysis are detailed in the requirements.txt file in the repository. These can be installed using pip:

`pip install -r requirements.txt`

or by creating a specific anaconda environment:

`conda create --name itEMD --python 3.9.4 --file requirements.txt`

# Notes
There are two options for re-creating the analysis. Fabus2021_ (Simulations, LFP, MEG).py scripts produce data for figures from skratch into Data/. In these, data_dir needs to point to where MEG/LFP data has been downloaded. Alternatively, Data_prebaked/ can be used as it contains summary analysis data produced by these scripts.

Fabus2021_Fig*.py produces individual figures. It can point to Data_prebaked/ or Data/ and re-produce figures from summary analysis data.

If you are interested in trying itEMD for decomposition of your own data, all you need is the itEMD.py script from /Tools with its use documented in the script routines and exemplified in the main scripts. 
