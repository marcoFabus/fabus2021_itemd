#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Routines:
    simulate_fig12_burst
    prepare_fig
    plot_psd
    rmse
    
@author: MSFabus
"""
import matplotlib.pyplot as plt
from Tools import sim, analysis
import numpy as np
from scipy.signal import welch
from scipy import interpolate

def simulate_fig12_burst():
    """
    Figure that simulates 2s of 4Hz non-sinusoidal delta = 30Hz burst.

    Returns
    -------
    x : numpy array of simulated signal

    """
    ##Simulate 2s of iterated sine signal
    sim_ = sim.sim(t=2, sample_rate=512)
    f0 = 4
    f_m = 2
    am = np.sin(2*np.pi*f_m*(sim_.time_vect-0.2))
    am[am < 0] = 0
    am[int(sim_.sample_rate*0.6) : ] = 0
    
    x = sim_.sim_sig(N_comp=2, amps=[1, 1], f=[f0, 30], shape=['it_sin', 'sin'], 
              noise=True, amp_mod=[1, am],
              noise_type='white', a_noise=1/16, it_sin_order=8, seed=0)
    
    return x


def prepare_fig(y_all, comparison='best', contrast='higher',
                H1=2, H0=[0,1], smoothing_win=3, p_lim=0.01):
    """
    Function to test a vector against best of other vectors.

    Parameters
    ----------
    y_all : numpy array of all methods x parameter x iterations values.
    comparison : string, optional
        'best' or 'single'. Best chooses contrast (higher/lower) of H0 columns.
        The default is 'best'.
    contrast : string, optional
        'lower' / 'higher'. The default is 'higher'.
    H1 : int, optional
        Column number of H1 method. The default is 3.
    H0 : list of ints, optional
        list of column numbers to test against. The default is [0,1,2].
    smoothing_win : int, optional
        optional moving mean smoothing. The default is 3.
    p_lim : float, optional
        p-value for significance in hypothesis test after Bonferroni correction.
        The default is 0.01.

    Returns
    -------
    list
        [mean traces, standard errors, H0_rejected region].

    """
    
    N1 = y_all.shape[0]
    N2 = y_all.shape[2]
    y = np.nanmean(y_all, axis=0)
    y_er = np.nanstd(y_all, axis=0) / np.sqrt(N1)
    
    if smoothing_win > 0:
        y = analysis.running_filt(y, smoothing_win, mode='mean')
        y_er = analysis.running_filt(y_er, smoothing_win, mode='mean')
        
    if comparison == 'single':
        mEMD_res = y_all[:, H0, :]
        itEMD_res = y_all[:, H1, :]
        
        if contrast == 'higher':
            [pvalue, corrpvalue, H0_rej] = analysis.corr_ttest(itEMD_res, mEMD_res, 
                                                                  verbose=True, 
                                                                  method='one-sided', p_lim=p_lim)
        if contrast == 'lower':
            [pvalue, corrpvalue, H0_rej] = analysis.corr_ttest(mEMD_res, itEMD_res, 
                                                      verbose=True, 
                                                      method='one-sided', p_lim=p_lim)
    if comparison == 'best':
        #Compare itEMD with best of each method 
        itEMD_res = y_all[:, H1, :]
        
        if contrast == 'higher':
            y_argmax = np.argmax(y[H0, :], axis=0)
            y_max_all = np.zeros((N1, N2))
            for i in range(N2):
                y_max_all[:, i] = y_all[:, H0[y_argmax[i]], i]
                
            [pvalue, corrpvalue, H0_rej] = analysis.corr_ttest(itEMD_res, y_max_all, 
                                                                  verbose=True, 
                                                                  method='one-sided',
                                                                  p_lim=p_lim)
        if contrast == 'lower':
            y_argmin = np.argmin(y[H0, :], axis=0)
            y_min_all = np.zeros((N1, N2))
            for i in range(N2):
                y_min_all[:, i] = y_all[:, H0[y_argmin[i]], i]
            [pvalue, corrpvalue, H0_rej] = analysis.corr_ttest(y_min_all, itEMD_res, 
                                                      verbose=True, 
                                                      method='one-sided', p_lim=p_lim)
    return [y, y_er, H0_rej]


def plot_psd(data, sample_rate, nperseg=None, return_psd=False, xlim=None,
             ylim=None):
    """
    Function that generates the power spectrum of a 1D time-series using 
    Welch's method.'
    
    """
    if nperseg is None:
        nperseg = 4 * sample_rate
    freqs, psd = welch(data, sample_rate, nperseg=nperseg, scaling='spectrum')
    plt.rc('font', size=36)
    fig, ax = plt.subplots(figsize=(12,8))
    ax.semilogy(freqs,psd, color='k', lw=3)
    
    if xlim is None:
        ax.set_xlim([0.5, 20])
    else:
        ax.set_xlim(xlim)
        
    if ylim is not None:
        ax.set_ylim(ylim)
    else:
        ax.set_ylim([psd.min()*0.9, psd.max() * 1.1])
    ax.grid(True)
    ax.set_xlabel('Freq [Hz]')
    ax.set_ylabel('Spectral Density')
    
    if return_psd:
        plt.close(fig=fig)
        return [freqs, psd]
    return ax


def rmse(predictions, targets):
    """
    Function to calculate RMSE between two arrays.

    """
    return np.sqrt(((predictions - targets) ** 2).mean())


def resample(x, fn, interp_x):
    #Resample fn along interp_ph for a given (IP, fn) pair
    i_fn = interpolate.interp1d(x, fn, fill_value="extrapolate")
    return i_fn(interp_x)


def plot_imfs(imfs, sample_rate, fig, ax1, pos, fontsize=8, nticks=3,
              xlab=True, labelpad=5, lw=1):
    """
    Function to plot IMFs in a specific set of axes.

    """
    ax1.axis('off')
    
    nplots = imfs.shape[1] + 1
    time_vect = np.linspace(0, imfs.shape[0]/sample_rate, imfs.shape[0])
    
    mx = np.abs(imfs).max()
    mx_sig = np.abs(imfs.sum(axis=1)).max()
    
    plt.rc('font', size=8)
    left, bottom, width, height = pos
    height /= nplots 
    ax = fig.add_axes([left, bottom, width, height])
    
    ax.yaxis.get_major_locator().set_params(integer=True)
    for tag in ['top', 'right', 'bottom']:
        ax.spines[tag].set_visible(False)
    ax.plot((time_vect[0], time_vect[-1]), (0, 0), color=[.5, .5, .5])
    ax.plot(time_vect, imfs.sum(axis=1), 'k', lw=lw)
    ax.tick_params(axis='x', labelbottom=False, labelsize=fontsize)
    ax.tick_params(axis='y', labelsize=fontsize)
    ax.set_xlim(time_vect[0], time_vect[-1])
    ax.set_ylim(-mx_sig * 1.1, mx_sig * 1.1)
    #ax.set_ylim(-5, 5)
    ax.set_ylabel('Signal', rotation=90, labelpad=labelpad, fontsize=fontsize)
    plt.locator_params(axis='x', nbins=nticks)
    plt.locator_params(axis='y', nbins=3)
    
    cmap = plt.cm.Dark2
    cols = cmap(np.linspace(0, 1, imfs.shape[1] + 1))
    
    #ax.axis('off')
    
    for ii in range(1, nplots):
        bottom -= 1.2*height
        ax = fig.add_axes([left, bottom, width, height])
        
        for tag in ['top', 'right', 'bottom']:
            ax.spines[tag].set_visible(False)
        ax.plot((time_vect[0], time_vect[-1]), (0, 0), color=[.5, .5, .5],
                lw=0.5)
        ax.plot(time_vect, imfs[:, ii - 1], color=cols[ii, :], lw=lw)
        ax.set_xlim(time_vect[0], time_vect[-1])
        ax.set_ylim(-mx * 1.03, mx * 1.03)
        ax.set_ylim(-1.05, 1.05)
        ax.yaxis.get_major_locator().set_params(integer=True)
        ax.set_ylabel('IMF-{0}'.format(ii), rotation=90, labelpad=labelpad, 
                      fontsize=fontsize)
        #ax.axis('off')
    
        if ii < nplots - 1:
            ax.tick_params(axis='x', labelbottom=False)
            ax.tick_params(axis='y', labelsize=fontsize)
            #ax.xaxis.get_major_locator().set_params(integer=True)
            plt.locator_params(axis='x', nbins=nticks)
        else:
            pass
            if xlab:
                ax.set_xlabel('Time [s]', fontsize=1.75*fontsize, labelpad=labelpad)
            else:
                ax.tick_params(axis='x', labelbottom=False)
            ax.tick_params(axis='x', labelsize=1.5*fontsize)
            ax.tick_params(axis='y', labelsize=fontsize)
            #ax.xaxis.get_major_locator().set_params(integer=True)
            plt.locator_params(axis='x', nbins=nticks)
    return