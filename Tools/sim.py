#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script with class:
    sim: simulated signal object
    
@author: MSFabus
"""
import matplotlib.pyplot as plt
import numpy as np
import colorednoise as cn

class sim():
    """ 
    Routines:
    sim_noise
    sim_sig
    plot_sig
    """
    
    def __init__(self, t=2, sample_rate=256):
        
        self.N_s = t * sample_rate
        self.sample_rate = sample_rate
        self.t = t 
        self.time_vect = np.linspace(0, self.t, self.N_s)
        
        
    def sim_noise(self, **kwargs):
        
        """
        Function to simulate coloured gaussian noise.
        """
        if 'noise_type' in kwargs:
            noise_type = kwargs["noise_type"]
            a_noise = kwargs["a_noise"]
        else:
            noise_type = 'white'
            a_noise = 1
        
        if "seed" in kwargs:
            np.random.seed(kwargs["seed"])
        else:
            np.random.seed(seed=None)
        
        if noise_type == 'white':
            noise = np.random.normal(0, a_noise, self.N_s) #n.b. was *a_noise
            return noise
        if noise_type == 'pink':
            beta = 1
        if noise_type == 'brown':
            beta = 2 
            
        e = cn.powerlaw_psd_gaussian(beta, self.N_s, fmin=0.1/self.sample_rate)
        
        return a_noise * e
                
    
    def sim_sig(self, N_comp=2, amps=[5, 10], f=[12, 4], shape=['sin', 'it_sin'], 
                amp_mod=[1, 1], noise=True, **kwargs):
        """
        Function to generate simulated signal.

        Parameters
        ----------
        N_comp : int, optional
            Number of signal components. The default is 2.
        amps : list of float, optional
            Amplitudes of each signal component. The default is [5, 10].
        f : list of float, optional
            frequencies of each signal component in Hz. The default is [12, 4].
        shape : list of strings, optional
            Shape type of signal components, 'sin' or 'it_sin'.
            The default is ['sin', 'it_sin'].
        amp_mod : list of floats orarrays , optional
            Optional amplitude modulation. Either matches length of expected
            signal or is a single float. The default is [1, 1].
        noise : bool, optional
            Include noise: True/False. The default is True.
        **kwargs : optional keyword arguments
            it_sin_order: order of iterated sine
            noise_type: white, pink, brown
            seed: random seed for noise
            noise_amp: RMS of noise

        Returns
        -------
        1D numpy array
            Simulated signal vector.

        """
        
        if "it_sin_order" in kwargs:
            N_its = kwargs["it_sin_order"]
        else:
            N_its = 6
            
        self.N_comp = N_comp
        self.noise = noise
        
        self.x = np.zeros((self.N_comp, self.N_s))
        self.sig = np.zeros(self.N_s)
        
        for i in range(self.N_comp):
            self.x[i, :] = np.sin(2*np.pi*f[i]*self.time_vect)
            
            if shape[i] == 'it_sin':
                for j in range(N_its):
                    self.x[i, :] = np.sin(self.x[i, :])
                self.x[i, :] /= np.amax(self.x[i, :])
            self.x[i, :] *= amps[i] * amp_mod[i]
                    
        self.sig = np.sum(self.x, axis=0) 
        
        if noise == True:
            self.x_n = self.sim_noise(**kwargs)
            self.sig += self.x_n
            
        return self.sig
    
    
    def plot_sig(self, secs=[1,2]):
        """
        Function to plot signal simulated by the sim class.

        Parameters
        ----------
        secs : list of floats, optional
            Time limits for signal plot. The default is [1,2].

        Returns
        -------
        None.

        """
        N_plots = 1 + self.N_comp + int(self.noise)
        idx = list(range(int(secs[0]*self.sample_rate), int(secs[1]*self.sample_rate)))
        plt.rc('font', size=24) 
        fig, axs_all = plt.subplots(N_plots, 1, figsize=(12, N_plots*4))
        axs = axs_all.flatten()
        axs[0].plot(self.time_vect[idx], self.sig[idx])
        axs[0].set_ylabel('Signal')
        
        for i in range(1, N_plots-int(self.noise)):
            ax = axs[i]
            ax.plot(self.time_vect[idx], self.x[i-1, idx])
            ax.set_ylabel('Tone %s' %i)
            
        if self.noise:
            axs[-1].plot(self.time_vect[idx], self.x_n[idx],)
            axs[-1].set_ylabel('Noise')
            
        axs[-1].set_xlabel('Time [s]')
        plt.tight_layout()
        plt.show() 